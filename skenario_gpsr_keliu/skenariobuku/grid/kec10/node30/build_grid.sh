 #!/bin/bash

 node_src=28;   # node sender
 node_dst=29;

 node_src_X=174.73
 node_src_Y=101.1
 node_src_Z=0

 node_dst_X=603.05;  # was 443.05
 node_dst_Y=607.3;   # was 297.3
 node_dst_Z=0

 # export SUMO_HOME (needed)
 SUMO_HOME=/usr/share/sumo
 export SUMO_HOME=$SUMO_HOME


for i in `seq 1 10`;
do
	mkdir -p skenario$i
    cd skenario$i
	echo skenario$i
		netgenerate --grid --grid.x-number 12 --grid.y-number 12 --grid.x-length 90 --grid.y-length 90 --default.speed=10 --tls.guess=1 --output-file=map.net.xml
		python /usr/share/sumo/tools/randomTrips.py -n map.net.xml -e 28 --intermediate=500 -l --seed $RANDOM --trip-attributes="departLane=\"best\" departSpeed=\"max\" departPos=\"random_free\" color=\"1,0,0\"" –o trips.trips.xml
		duarouter -n map.net.xml -t trips.trips.xml -o route.rou.xml --ignore-errors --repair
		# make a rule time start 0 and stop 200
		sumo -b 0 -e 200 -n map.net.xml -r route.rou.xml --fcd-output scenario.xml
		# export to activity and mobility file
		python /usr/share/sumo/tools/traceExporter.py --fcd-input=scenario.xml --ns2mobility-output map.mobility.tcl
		# add source and recv node to scenario
		# add source and recv node to scenario
		mv map.mobility.tcl old.txt
		echo -e '$node_('$node_src') set X_ '$node_src_X'\n$node_('$node_src') set Y_ '$node_src_Y'\n$node_('$node_src') set Z_ '$node_src_Z'\n$node_('$node_src') color cyan\n$node_('$node_dst') set X_ '$node_dst_X'\n$node_('$node_dst') set Y_ '$node_dst_Y'\n$node_('$node_dst') set Z_ '$node_dst_Z'\n$node_('$node_dst') color red\n$ns_ at 0.0 "$node_('$node_src') color cyan"\n$ns_ at 0.0 "$node_('$node_dst') color red"\n$ns_ at 0.0 "$node_('$node_src') setdest '$node_src_X' '$node_src_Y' '$node_src_Z'"\n$ns_ at 0.0 "$node_('$node_dst') setdest '$node_dst_X' '$node_dst_Y' '$node_dst_Z'"' > tmp.txt
		(cat tmp.txt && cat old.txt) > map.mobility.tcl
		rm tmp.txt old.txt
		sed -i 's/-//g' map.mobility.tcl
	cd ..
done	
