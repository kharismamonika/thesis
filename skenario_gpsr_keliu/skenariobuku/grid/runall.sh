 #!/bin/bash
 rm evalpdrbyspeed.log
 rm evalpdrbynode.log
 rm evale2ebyspeed.log
 rm evale2ebynode.log
 rm evalrobyspeed.log
 rm evalrobynode.log
 for j in {5..25..5}
 do 
 	cd kec$j
 	for i in {30..100..20}
 	do 
 		cd node$i
 		echo skenario kec $j- node $i >> ../../../evalpdr.log
 		for k in `seq 1 10`;
 		do
 			cd skenario$k
 			echo skenario kec $j- node $i-$k
 			rm debug.log
 			rm posisi.log
 			nsgpsrcurrspeed script.tcl
 			#nsgpsrori script.tcl
 			#nsgpsravgspeed script.tcl
 			#nsgpsravgspeedalfa0.1 script.tcl
 			rm *.nam
 			#rm gpsr_tracefile.tr
 			cd ..
 		done
 		cd ..
 	done
 	cd ..
 done

#Eval by Speed
for j in {5..25..5}
 do 
 	cd kec$j
 	for i in {30..100..20}
 	do 
 		cd node$i
 		echo skenario kec $j node $i >> ../../evalpdrbyspeed.log
 		echo skenario kec $j node $i >> ../../evale2ebyspeed.log
 		echo skenario kec $j node $i >> ../../evalrobyspeed.log
 		#echo skenario kec $j node $i >> ../../evalroutebyspeed.log
 		for k in `seq 1 10`;
 		do
 			cd skenario$k
 			awk -f ../../../pdr.awk gpsr_tracefile.tr >> ../../../evalpdrbyspeed.log
 			awk -f ../../../e2e.awk gpsr_tracefile.tr >> ../../../evale2ebyspeed.log
 			awk -f ../../../ro.awk gpsr_tracefile.tr >> ../../../evalrobyspeed.log
 			#awk -f ../../../cek_route.awk posisi.log >> ../../../evalroutebyspeed.log
 			cd ..
 		done 
 		cd ..
 	done
 	cd ..
 done

#Eval by Node
 for i in {30..100..20}
 do 
 	for j in {5..25..5}
 	do 
 		cd kec$j
 		cd node$i
 		echo skenario kec $j node $i >> ../../evalpdrbynode.log
 		echo skenario kec $j node $i >> ../../evale2ebynode.log
 		echo skenario kec $j node $i >> ../../evalrobynode.log
 		#echo skenario kec $j node $i >> ../../evalroutebynode.log
 		for k in `seq 1 10`;
 		do
 			cd skenario$k
 			awk -f ../../../pdr.awk gpsr_tracefile.tr >> ../../../evalpdrbynode.log
 			awk -f ../../../e2e.awk gpsr_tracefile.tr >> ../../../evale2ebynode.log
 			awk -f ../../../ro.awk gpsr_tracefile.tr >> ../../../evalrobynode.log
 			#awk -f ../../../cek_route.awk posisi.log >> ../../../evalroutebynode.log

 			#rm gpsr_tracefile.tr
 			cd ..
 		done
 		cd ..
 		cd ..
 	done
 done


for j in {5..25..5}
 do 
 	cd kec$j
 	for i in {30..100..20}
 	do 
 		cd node$i
 		for k in `seq 1 10`;
 		do
 			cd skenario$k
 			rm gpsr_tracefile.tr
 			cd ..
 		done
 		cd ..
 	done
 	cd ..
 done