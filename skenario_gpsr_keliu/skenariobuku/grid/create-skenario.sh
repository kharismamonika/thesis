#!/bin/bash

nn=30;
node_src=$(($nn-2));   # node sender
node_dst=$(($nn-1));

cd numnode$nn
for i in `seq 1 10`;
do
    cd skenario$i
	echo skenario$i
		cp ../../template_script.tcl script.tcl 
		cp ../../gpsr.tcl gpsr.tcl
		cp ../../template_cbr.tcl cbr.tcl 
		# create script

		sed -i 's/X_PLACEHOLDER/1000/g' script.tcl;
		sed -i 's/Y_PLACEHOLDER/1000/g' script.tcl;
		sed -i 's/IFQ_PLACEHOLDER/512/g' script.tcl;
		sed -i 's/SEED_PLACEHOLDER/0.0/g' script.tcl;
		sed -i 's/PROTOCOL_PLACEHOLDER/gpsr/g' script.tcl;
		sed -i 's/NODE_PLACEHOLDER/'$nn'/g' script.tcl;

		sed -i 's#CBRFILE_PLACEHOLDER#"./cbr.tcl"#g' script.tcl;
		sed -i 's#SCEN_PLACEHOLDER#"./map.mobility.tcl"#g' script.tcl;

		sed -i 's/STOP_PLACEHOLDER/200/g' script.tcl;
		sed -i 's/OUTPUT_PLACEHOLDER/gpsr_tracefile.tr/g' script.tcl;
		sed -i 's/NAM_PLACEHOLDER/gpsr_tracefile.nam/g' script.tcl;

		sed -i 's#NSPATH_PLACEHOLDER#"/home/ririspc/thesis/nsgpsrkeliu/ns-allinone-2.35/ns-2.35"#g' script.tcl;

		sed -i 's/PLANAR_PLACEHOLDER/1/g' script.tcl;
		sed -i 's/HELLO_PLACEHOLDER/5.0/g' script.tcl;

		sed -i "s/DST/$node_dst/g" cbr.tcl
		sed -i "s/SRC/$node_src/g" cbr.tcl

	cd ..
done	

cd ..