# AWK Script for Packet Delivery Calculation for OLD Trace Format

BEGIN {
  
}

{

   if(ROUTE[$2]=="")ROUTE[$2]=$6;
   ROUTE[$2] = ROUTE[$2] ">" $10;
}
END {
  PROCINFO["sorted_in"] = "@val_str_desc";
  for(key in ROUTE){
   	# printf "%s %s\n",key,ROUTE[key];
   	# printf "%s %s\n",key,ROUTE[key];
   	if(USE_ROUTE[ROUTE[key]]=="")USE_ROUTE[ROUTE[key]]=0;
   	USE_ROUTE[ROUTE[key]]++;
  }
  for(key in USE_ROUTE){
  	printf "%s %s\n",key,USE_ROUTE[key];
  }
}

