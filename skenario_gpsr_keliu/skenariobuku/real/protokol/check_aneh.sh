 #!/bin/bash

input="./evalpdr_ori.log"
input_avg="./evalpdr.log"
input_cur="./evalpdr_cur.log"

skenario_num_ori=0;
skenario_num_avg=0;
while IFS= read -r line
do
  A="$(cut -d' ' -f1 <<<"$line")"
  if [[ $A == 'skenario' ]]; then
  	node_num_ori="$(cut -d' ' -f3 <<<"$line")"
  	skenario_num_ori=0;
  fi
  if [[ $skenario_num_ori -gt 0 ]]; then
  	pdr_floor_ori="$(cut -d'.' -f1 <<<"$line")"
  	if [[ $pdr_floor_ori -lt 10 ]]; then
  		echo "retry gpsr ori node $node_num_ori skenario $skenario_num_ori with value $line"
  		cd "node$node_num_ori/skenario$skenario_num_ori/"
  		bash "./singlebuild.sh"
  		cd ../../
      continue
  	fi

  	while IFS= read -r line_avg
  	do
  	  A="$(cut -d' ' -f1 <<<"$line_avg")"
  	  if [[ $A == 'skenario' ]]; then
  	  	node_num_avg="$(cut -d' ' -f3 <<<"$line_avg")"
  	  	skenario_num_avg=0;
  	  fi
  	  if [[ $node_num_avg -eq $node_num_ori ]] && [[ $skenario_num_avg -eq $skenario_num_ori ]]; then
  	  	pdr_floor_avg="$(cut -d'.' -f1 <<<"$line_avg")"
  	  	if [[ $pdr_floor_avg -lt $pdr_floor_ori ]]; then
  	  		echo "retry gpsr avg node $node_num_avg skenario $skenario_num_avg with value $line_avg < $line"
  	  		cd "node$node_num_ori/skenario$skenario_num_ori/"
	  		  bash "./singlebuild.sh"
	  		  cd ../../
          continue
  	  	fi
        # check cur and avg
        while IFS= read -r line_cur
        do
          A="$(cut -d' ' -f1 <<<"$line_cur")"
          if [[ $A == 'skenario' ]]; then
            node_num_cur="$(cut -d' ' -f3 <<<"$line_cur")"
            skenario_num_cur=0;
          fi
          if [[ $node_num_cur -eq $node_num_avg ]] && [[ $skenario_num_cur -eq $skenario_num_avg ]]; then
            pdr_floor_cur="$(cut -d'.' -f1 <<<"$line_cur")"
            if [[ $pdr_floor_cur -gt $pdr_floor_avg ]]; then
              echo "retry gpsr cur node $node_num_cur skenario $skenario_num_cur with value $line_cur > $line_avg"
              cd "node$node_num_ori/skenario$skenario_num_ori/"
             bash "./singlebuild.sh"
             cd ../../
             continue
            fi
            
          fi
          skenario_num_cur=$((skenario_num_cur+1));
        done < "$input_cur"
  	  	
  	  fi
      
      

  	  skenario_num_avg=$((skenario_num_avg+1));
  	done < "$input_avg"

  fi
  skenario_num_ori=$((skenario_num_ori+1));
done < "$input"