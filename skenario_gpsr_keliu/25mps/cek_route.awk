# AWK Script for Packet Delivery Calculation for OLD Trace Format

BEGIN {
  
}

{

   if(ROUTE[$2]=="")ROUTE[$2]="Packet "$2" "$6;
   ROUTE[$2] = ROUTE[$2] ">" $10 ;
}
END {
  PROCINFO["sorted_in"] = "@val_str_desc";
  for(key in ROUTE){
   	printf " %s - %s\n",key,ROUTE[key];
  }
}

