# AWK Script for Packet Delivery Calculation for OLD Trace Format

BEGIN {
MAC=0;
MAC_GPSR=0;
MAC_CBR_COL=0;
AGT=0;
RTR= 0;
}

{

# count packet dropped
  if($1=="D" && $4=="AGT" && $7=="cbr")
   {
    AGT++;
   }

  else if($1=="D"  && $4=="MAC" && $7=="cbr")
   {
     MAC++;
   }
     else if($1=="D"  && $4=="MAC" && $5=="COL" && $7=="gpsr")
   {
     MAC_GPSR++;
   }

     else if($1=="D"  && $4=="MAC" && $5=="COL" && $7=="cbr")
   {
     MAC__CBR_COL++;
   }

   else if($1=="D"  && $4=="RTR"  && $7=="gpsr")
    {
      RTR++;
    }
 
}
END {
 printf " MAC:%d\n",MAC;
 printf " MAC GPSR COL:%d\n",MAC_GPSR;
 printf " MAC CBR COL:%d\n",MAC_CBR_COL;
 printf "AGT:%d",AGT;
 printf "\n RTR:%d \n",RTR;
 #printf "\n Packet Delivery Ratio:%.2f\n",(received/sent)*100;
 #printf "\n%.2f\n",(received/sent)*100;

}

