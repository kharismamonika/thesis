#ifndef quadratic_h
#define quadratic_h

#include "cellbuilder.h"

class QuadraticCellbuilder : public Cellbuilder
{
  public :
    QuadraticCellbuilder(double radiorange, double fieldsize_x, double fieldsize_y);

    
      int getCellID (position position);

      int getRegion (position position, int level);

      int getRC (int nodeid, int level, position position);

      position getPosition (int cellid);

      int getMaxLevel();
      
      void print(position p);
      void getNeighborCells(int cellid, int* number, int** result);
      

 private:
      double radiorange; // the transmission range
      double x;          // the size of the field (it's length)
      double y;          // the size of the field (it's heigth)
      int squarelength;     // the length and heigth of one square
      int cellsPerLine; // the number of cells which are on the same
                        // line in x-direction
      int cellsPerColumn;
      int maxLevel;     // the biggest level possible (on this level, 
      // there is only one region which consist of the whole simulation
      // area)
      inline bool isLineValid(int line) {
	if((0 <= line) && (line < cellsPerLine))
	  {
	    return true;
	  }
	else 
	  {
	    return false;
	  }
      };
      inline bool isColumnValid(int column) {
	if((0 <= column) && (column  < cellsPerColumn))
	  {
	    return true;
	  }
	else 
	  {
	    return false;
	  }	
      };
};


// this is the second, improved version of the QuadraticCellbuilder;
// it tries to concentrate the highest level RCs in the center of the
// area, it also contains some "bugfixes" which led to strange 
// behaviour in the first version
class QuadraticCellbuilderV2 : public Cellbuilder
{

  public :
    QuadraticCellbuilderV2(double radiorange, double fieldsize_x, double fieldsize_y);

    
      int getCellID (position position);

      int getRegion (position position, int level);

      int getRC (int nodeid, int level, position position);

      position getPosition (int cellid);

      int getMaxLevel();

      void getNeighborCells(int cellid, int* number, int** result);
      

 private:
      double radiorange; // the transmission range
      double x;          // the size of the field (it's length)
      double y;          // the size of the field (it's heigth)
      int squarelength;     // the length and heigth of one square
      int cellsPerLine; // the number of cells which are on the same
                        // line in x-direction
      int cellsPerColumn;
      int maxLevel;     // the biggest level possible (on this level, 
      // there is only one region which consist of the whole simulation
      // area)
      int numberOfCells; // the number of cells used in this simulation
      inline bool isLineValid(int line) {
	if((0 <= line) && (line < cellsPerLine))
	  {
	    return true;
	  }
	else 
	  {
	    return false;
	  }
      };
      inline bool isColumnValid(int column) {
	if((0 <= column) && (column  < cellsPerColumn))
	  {
	    return true;
	  }
	else 
	  {
	    return false;
	  }	
      };
      
};

#endif


