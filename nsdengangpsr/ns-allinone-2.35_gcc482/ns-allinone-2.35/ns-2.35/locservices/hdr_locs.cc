/*

  Contacts: 
  Michael Kaesemann (mikael@uni-mannheim.de)

  Permission to use, copy, modify, and distribute this software and its
  documentation in source and binary forms is hereby granted, provided 
  that the above copyright notice appears in all copies and the author is 
  acknowledged in all documentation pertaining to any such copy or
  derivative work.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*/

//#include <stdio.h>
#include "hdr_locs.h"

int hdr_locs::offset_;

class LOCSHeaderClass : public PacketHeaderClass {
public:
    LOCSHeaderClass() : PacketHeaderClass("PacketHeader/LOCS",sizeof(hdr_locs)) {
	bind_offset(&hdr_locs::offset_);
    }
} class_locshdr;
