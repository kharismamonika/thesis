/*

  Contacts: 
  Michael Kaesemann (mikael@uni-mannheim.de)

  Permission to use, copy, modify, and distribute this software and its
  documentation in source and binary forms is hereby granted, provided 
  that the above copyright notice appears in all copies and the author is 
  acknowledged in all documentation pertaining to any such copy or
  derivative work.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*/

#ifndef _LS_Queued_Timer_cc
#define _LS_Queued_Timer_cc

#include "ls_queued_timer.h"
#include "ls_timer_heap.h"
#include "locservice.h"

/*****************/
/* Queued Timer */
/*****************/

QueuedTimer::QueuedTimer(int size)
    : TimerHandler()
{ 
    in_progress = NULL; 
    heap_ = new TimerHeap(size,this); 
    parent = -2;
    warn_multiple = true;
}

QueuedTimer::QueuedTimer(int size, bool mwarn)
    : TimerHandler()
{ 
    in_progress = NULL; 
    heap_ = new TimerHeap(size,this); 
    parent = -2;
    warn_multiple = mwarn;
}

QueuedTimer::~QueuedTimer() {
    delete heap_;
}

void QueuedTimer::shutdown() {
	
    // Get rid of pending event
    if (in_progress != NULL) {
      if (in_progress->info != NULL) { deleteInfo(in_progress->info); }
      delete in_progress;
      in_progress = NULL;
      cancel();
    }
    // Clean Heap
    heap_->clean();
    return; 
}

void QueuedTimer::add(int32_t key_, double delay_, void* info_ /* = NULL */) {

    // Prune invalid KEYs
    if (key_ < 0) { return; }
    add((unsigned int)key_,delay_,info_);

}

void QueuedTimer::add(unsigned int key_, double delay_, void* info_ /* = NULL */) {

    // Add new event to the Heap
    struct TimerHeapEntry* reqevent = new TimerHeapEntry;
    reqevent->key = key_;
    // Heap is absolute time addressed
    reqevent->ts = (Scheduler::instance().clock() + delay_);
    reqevent->info = info_;
    heap_->insert(reqevent);
    notify();

#ifndef NDEBUG

    /* 
       In Debugging Mode print a Warning if a key
       is used multiple times - mk
    */
    unsigned int usage_;
    if (warn_multiple && ((usage_ = countEntries(key_)) > 1)) {
      printf("QueuedTimer Warning: Key %d used %d times !\n", key_, usage_);
    }

#endif
}

void QueuedTimer::expire(Event *e) {

    // Keep Info about Event
    local_key = in_progress->key;
    local_info = in_progress->info;

    // Kill timed out Event
    delete in_progress;
    in_progress = NULL;
  
    handle();

    if (heap_->empty()) { return; }

    // In case handle did not call add and thus no
    //  in_progress event is scheduled, we should 
    //  schedule the next event ourselves
    if (in_progress == NULL) {
	in_progress = heap_->extract_min();
	double now = Scheduler::instance().clock();
	// Make sure all expired timers are removed
	while (in_progress->ts < now) { 
	    printf("QueuedTimer Warning: Skipped Queued Timer !\n");
	    if (in_progress->info != NULL) { deleteInfo(in_progress->info); }
	    delete in_progress;
	    in_progress = heap_->extract_min();
	    if (in_progress == NULL) { return; }
	}
	resched(in_progress->ts - now);
    }
}

bool QueuedTimer::remove(int32_t key_) {

    // Prune invalid KEYs
    if (key_ < 0) { return false; }
    return remove((unsigned int)key_);

}

bool QueuedTimer::remove(unsigned int key_) {

    // Remove element from queue
    bool removed = heap_->remove(key_);

    if (!in_progress) { return (removed); }

    // Check if event to key_ is pending
    if (in_progress->key == key_) {

	removed = true;

	// Event pending, kill it and schedule
	//  next event if one exists
	if (in_progress->info != NULL) { deleteInfo(in_progress->info); }
	delete in_progress;
	in_progress = NULL;
	cancel();
    
	if (heap_->empty()) { return (removed); }

	in_progress = heap_->extract_min();
	double now = Scheduler::instance().clock();
	// Make sure all expired timers are removed
	while (in_progress->ts < now) { 
	    printf("QueuedTimer Warning: Skipped Queued Timer !\n");
	    if (in_progress->info != NULL) { deleteInfo(in_progress->info); }
	    delete in_progress;
	    in_progress = heap_->extract_min();
	    if (in_progress == NULL) { return (removed); }
	}
	sched(in_progress->ts - now);
    }

    return (removed);
}

void* QueuedTimer::extract(unsigned int key_, double* ttime_ /*= NULL*/) {

  // Init ttime_ to -1.0
  if (ttime_ != NULL) { (*ttime_) = -1.0; }

  // If no event is pending, no match can be found
  if (!in_progress) { return NULL; }

  void* tmp = NULL;
  double tmptime = -1.0;

  // Check if event to key_ is pending
  if (in_progress->key == key_) {

    // Event pending, kill it and schedule
    //  next event if one exists
    tmp = in_progress->info;
    tmptime = in_progress->ts;
    delete in_progress;
    in_progress = NULL;
    cancel();
    
    if (ttime_ != NULL) { (*ttime_) = tmptime; }

    if (heap_->empty()) { return (tmp); }

    in_progress = heap_->extract_min();
    double now = Scheduler::instance().clock();
    // Make sure all expired timers are removed
    while (in_progress->ts < now) { 
      printf("QueuedTimer Warning: Skipped Queued Timer !\n");
      if (in_progress->info != NULL) { deleteInfo(in_progress->info); }
      delete in_progress;
      in_progress = heap_->extract_min();
    }
    sched(in_progress->ts - now);
    return (tmp);
  }

  // Check queue for event to key_
  tmp = heap_->extract(key_, &tmptime);
  if (tmp == NULL) { return NULL; }
  else { 
    if (ttime_ != NULL) { (*ttime_) = tmptime; }
    return (tmp); 
  }
}

void* const QueuedTimer::peek(unsigned int key_) {

  // If no event is pending, no match can be found
  if (!in_progress) { return NULL; }

  // Check if event to key_ is pending
  if (in_progress->key == key_) { return in_progress->info; }

  // Check queue for event to key_
  return (heap_->peek(key_));
}

bool QueuedTimer::queued(unsigned int key_) {
    
    // Check if element is in queue
    bool queued = heap_->find(key_);

    if (!in_progress) { return (queued); }

    // Check if event to key_ is pending
    if (in_progress->key == key_) { queued = true; }
    
    return (queued);
}

void QueuedTimer::notify() {

    // Check if we can schedule an event

    if (!in_progress) { // No Timer pending

	if (heap_->empty()) { return; }

	in_progress = heap_->extract_min();
	double now = Scheduler::instance().clock();
	// Make sure all expired timers are removed
	while (in_progress->ts < now) { 
	    printf("QueuedTimer Warning: Skipped Queued Timer !\n");
	    if (in_progress->info != NULL) { deleteInfo(in_progress->info); }
	    delete in_progress;
	    in_progress = heap_->extract_min();
	    if (in_progress == NULL) { return; }
	}
	resched(in_progress->ts - now);
	
    }else{ // Timer pending

	if (heap_->empty()) { return; }

	if ((heap_->tell_min())->ts <= in_progress->ts) {

	    // MinReq should be scheduled earlier, reorder
	    heap_->insert(in_progress);
	    in_progress = heap_->extract_min();
	    double now = Scheduler::instance().clock();
	    // Make sure all expired timers are removed
	    while (in_progress->ts < now) { 
		printf("QueuedTimer Warning: Skipped Queued Timer !\n");
		if (in_progress->info != NULL) { deleteInfo(in_progress->info); }
		delete in_progress;
		in_progress = heap_->extract_min();
		if (in_progress == NULL) { return; }
	    }
	    resched(in_progress->ts - now);
	}
    }
}

unsigned int QueuedTimer::countEntries(unsigned int key_) {
    
    // Count entries in queue
    unsigned int counter = heap_->count(key_);

    if (!in_progress) { return (counter); }

    // Check if event to key_ is pending
    if (in_progress->key == key_) { counter++; }
    
    return (counter);
}

void QueuedTimer::printQueue() {

    if (in_progress != NULL) {
	printf("InProgress (%d,%f,%d)\n",in_progress->key,in_progress->ts,(int*)in_progress->info);
    }else{
	printf("InProgress (NT,NT,NT)\n");
    }
    heap_->printTable();
}

/******************/
/* Heap Functions */
/******************/

void TimerHeap::insert(struct TimerHeapEntry* elem) {
    unsigned int i, par;

    if (heapsize == maxsize) { // Heap is full and needs to grow
	maxsize *= 2;
	struct TimerHeapEntry** tmp = new TimerHeapEntry*[maxsize];
	bzero (tmp, maxsize*sizeof(TimerHeapEntry*));
	bcopy (elems, tmp, heapsize*sizeof(TimerHeapEntry*));
	delete [] elems;
	elems = tmp;
    }

    i = heapsize++;
    par = parent(i);
    while ((i > 0) && (lessthan(elem,elems[par]))) { 
	elems[i] = elems[par];
	i = par;
	par = parent(i);
    }
    elems[i] = elem;
    return;
};

struct TimerHeapEntry* TimerHeap::extract_min() {
    struct TimerHeapEntry* min;   

    if (heapsize == 0)
	return NULL;
    min = elems[0];
    elems[0] = elems[--heapsize];
    elems[heapsize] = NULL;
    heapify(0);

    return min;
}

void TimerHeap::heapify(unsigned int i) {
    unsigned int	l, r, x;

    while (i < heapsize) {
	l = left(i); r = right(i);
	if (r < heapsize) {
	    if (lessthan(elems[l],elems[r])) x = l;
	    else                             x = r;
	} else
	    x = (l < heapsize ? l : i);
	if ((x != i) && lessthan(elems[x], elems[i])) {
	    swap(i, x); i = x;
	} else {
	    break;
	}
    }
}

void TimerHeap::removeElem(int elem) {
  for (elem; elem; elem = parent(elem)) {
    swap(elem, parent(elem));
  }
  struct TimerHeapEntry* tmpElem = extract_min();
  delete tmpElem;
}

bool TimerHeap::remove(unsigned int key_) {

    if (empty()) { return false; }

    // Delete all events
    // 07.03.02 [HMF] changed increment to decrement (didn't make sense to
    // Martin and me)

    bool found = false;

    for (int i = maxsize-1; i >= 0; i--) {
	if ((elems[i]!=NULL)&&(elems[i]->key == key_)) {
	    if (elems[i]->info != NULL) { timer->deleteInfo(elems[i]->info); }
	    removeElem(i);
	    found = true;
	}
    }
    return found;
}

unsigned int TimerHeap::count(unsigned int key_) {

    if (empty()) { return 0; }

    unsigned int counter = 0;

    for (int i = maxsize-1; i>=0; i--) {
      if ((elems[i]!=NULL)&&(elems[i]->key == key_)) {
	counter++;
      }
    }
    return counter;
}

void* TimerHeap::extract(unsigned int key_, double* ttime_) {

    if (ttime_ != NULL) { (*ttime_) = -1.0; }

    if (empty()) { return NULL; }

    // Note: Should more than one entry fit the key
    //       only the first found entry will be
    //       returned; all others remain
    void* tmp = NULL;
    for (int i = maxsize-1; i>=0; i--) {
	if ((elems[i]!=NULL)&&(elems[i]->key == key_)) {
	  if (ttime_ != NULL) { (*ttime_) = elems[i]->ts; }
	  if (elems[i]->info != NULL) { tmp = elems[i]->info; }
	  else { tmp = NULL; }
	  removeElem(i);
	  return tmp;
	}
    }
    
    // No fitting entry
    return NULL;
}

void* const TimerHeap::peek(unsigned int key_) {

    if (empty()) { return NULL; }

    // Note: Should more than one entry fit the key
    //       only the first found entry will be
    //       returned; all others remain
    for (int i = maxsize-1; i>=0; i--) {
	if ((elems[i]!=NULL)&&(elems[i]->key == key_)) {
	  return elems[i]->info;
	}
    }
    
    // No fitting entry
    return NULL;
}

bool TimerHeap::find(unsigned int key_) {

    if (empty()) { return false; }

    // Search all events
    for (int i = maxsize-1; i>=0; i--) {
	if ((elems[i]!=NULL)&&(elems[i]->key == key_)) {
	    return true;
	}
    }
    return false;
}

void TimerHeap::printTable() {
    printf("HeapTable (%d/%d): ",heapsize,maxsize);
    for (unsigned int i=0; i<maxsize; i++) {
	if (elems[i] != NULL)
	    printf(" (%d,%f,%d)",elems[i]->key,elems[i]->ts,(int*)elems[i]->info);
	else
	    printf(" (NN,NT,NT)");
    }
    printf("\n");
}

#endif //_LS_Queued_Timer_cc
