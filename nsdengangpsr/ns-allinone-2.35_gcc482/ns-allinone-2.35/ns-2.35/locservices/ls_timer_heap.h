/*

  Contacts: 
  Michael Kaesemann (mikael@uni-mannheim.de)

  Permission to use, copy, modify, and distribute this software and its
  documentation in source and binary forms is hereby granted, provided 
  that the above copyright notice appears in all copies and the author is 
  acknowledged in all documentation pertaining to any such copy or
  derivative work.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*/

#ifndef _LS_Request_Heap_h
#define _LS_Timer_Heap_h

#include "hdr_locs.h"

/****************/
/* Timer Heap */
/****************/

class TimerHeap {

 public:
    TimerHeap(int size, class QueuedTimer* timer_) {
	elems = new TimerHeapEntry*[size];
	bzero (elems, size*sizeof(TimerHeapEntry*));
	maxsize = size;
	heapsize = 0;
	inverted = false;
	timer = timer_;
    }

    ~TimerHeap() {
	for (int i = maxsize-1; i>=0; i--) {
	    if (elems[i]->info != NULL) { timer->deleteInfo(elems[i]->info); }
	}
   	delete [] elems;
    }

    void heapify(unsigned int i);
    void insert(struct TimerHeapEntry* elem);
    bool remove(unsigned int key_);
    void* extract(unsigned int key_, double* ttime_ = NULL);
    bool find(unsigned int key_);
    void* const peek(unsigned int key_);
    unsigned int count(unsigned int key_);
    void printTable();
    struct TimerHeapEntry* extract_min();
    inline struct TimerHeapEntry* tell_min() {
	return (heapsize > 0 ? elems[0] : NULL);
    };

    inline void invert() { if (heapsize==0) { inverted = !inverted; } }
    inline unsigned int	parent(unsigned int i)	{ return ((i - 1) / 2); }
    inline unsigned int	left(unsigned int i)	{ return ((i * 2) + 1); }
    inline unsigned int	right(unsigned int i)	{ return ((i + 1) * 2); }

    inline void clean() {
	for (unsigned int i=0; i<maxsize; i++)
	    elems[i] = NULL;
	heapsize = 0;
	inverted = false;
    }
    
    inline struct TimerHeapEntry* get(int i) {
	if (elems[i]==NULL)
	    return NULL;
	else
	    return elems[i];
    }

    inline bool empty() {
	if (heapsize==0) return true;
	else return false;
    }

    inline void iter_init() {
	heapiter = 0;
    }

    inline struct TimerHeapEntry* iter() {
	if (heapiter >= heapsize) {
	    heapiter = 0;
	    return NULL;
	} else {
	    return elems[heapiter++];
	}
    }

 private:
    struct TimerHeapEntry** elems;
    class QueuedTimer* timer;
    bool inverted;
    unsigned int maxsize;
    unsigned int heapsize;
    unsigned int heapiter;

    void removeElem(int elem);
  
    inline void swap(unsigned int i, unsigned int j) {
	TimerHeapEntry* helper = elems[i];
	elems[i] = elems[j];
	elems[j] = helper;
	return;
    };

    inline bool equal(struct TimerHeapEntry* q1, struct TimerHeapEntry* q2) {
	if ((q1->key == q2->key) &&
	    (q1->ts == q2->ts))
	    return true;
	else
	    return false;
    }

    inline bool lessthan(struct TimerHeapEntry* q1, struct TimerHeapEntry* q2) {
	if (inverted) {
	    if (q1->ts > q2->ts) { return true; }
	    else { return false; }
	}else{
	    if (q1->ts < q2->ts) { return true; }
	    else { return false; }
	}
    }
};
#endif //_LS_Timer_Heap_h






