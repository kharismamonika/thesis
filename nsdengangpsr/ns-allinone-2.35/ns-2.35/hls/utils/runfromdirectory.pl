#!/usr/bin/perl
my $recursive = "false";
my $use_peri  = "1";
my $mac_emu   = "1";
my $locs      = "3";
my $necessary_queries = "1200";

my $run_tcl = "/home/stud/kiess/space/ns-util/run.tcl";

##############
# main program
##############
parseCmdLine();

# recursive part
if($recursive eq "true") {
    print "recursive\n";

    opendir(DIRECTORY, '.');
    @entries = readdir(DIRECTORY);
    foreach $entry (@entries) {
	if(( -d $entry) && (!($entry =~ /\.+/o)) ) {
	    # select all directories which are != "." or ".."
	    print "processing $entry ...\n";
	    processDirectory($entry);
	}
    }
# end of recursive part
} else {
    print "non recursive\n";
    processDirectory(".");
}


sub processDirectory {
    my $entry = shift();
    chdir $entry;
    print "process $entry\n";
    opendir(WORKING_DIRECTORY, '.');

    
    my $numberOfNodes;
    my $connections;
    my $startuptime;
    my $communicationEndTime;
    my $x;
    my $y;
    my $speed;
    my $overallTime;
    my $sc = "";
    my $cp = "";

    my $hls_queries = 0;
    my $gls_queries = 0;
    
    
    @files = readdir(WORKING_DIRECTORY);
    foreach $file (@files) {
	if( -f $file) {
#	    print "$entry / $file\n";
	    
#cp-n400-c300-15-78.tcl
#sc-x2000-y2000-n400-s30-t80-MRD
	    # it is the communication pattern file
	    if($file =~ /cp-n(\d+)-(c(\d+)?)-(\d+)-(\d+)/o) {
		$numberOfNodes        = $1;

		$startuptime          = $4;
		$communicationEndTime = $5;
		$cp = $file;

		#print "$1 $2 $3 $4 $5\n";
		if($2 =~ /c(\d+)/o) {
		    $connections          = $1;
		} else {
		    $connections = 0;
		}
		print "matched $file\n";  
		}
	    if($file =~ /sc-x(\d+)-y(\d+)-n(\d+)-s(\d+)-t(\d+)/o) {
		$x = $1;
		$y = $2;
		$speed = $4;
		$overallTime = $5;
		$sc = $file;
		print "matched $file\n";
	    }

	    # match the evaluated file
	    if($file =~ /evaluated/o) {
		open(FILE, "$file");
		my $queries = 0;
		while (my $line = <FILE>) {
		    if($line =~ /Queries         : (\d+)/o){
			$queries = $1;
		    }
		}
#		print $queries . "\n";
		if(($file =~ /peri_evaluated/o) || ($file =~ /hls_evaluated/o)) {
		    $hls_queries = $queries;
		}
		if($file =~ /gls_evaluated/o) {
		    $gls_queries = $queries;
		}

#		print $file . "\n";
	    }
	}
    }
    
    # we have the information we need, now run ns...
    if(($sc ne "") &&($cp ne "")){
	my $filename;
	if($locs eq "3") {
	    $filename = "hls_trace.txt";
	    if($necessary_queries == $hls_queries) {
		print "$hls_queries queries in evaluated file, thus don't run simulations\n";
		chdir "..";
		return;
	    }
	} else {
	    $filename = "gls_trace.txt";	    
	    if($necessary_queries == $gls_queries) {
		print "$gls_queries queries in evaluated file, thus don't run simulations\n";
		chdir "..";
		return;
	    }
	}
	
	print "executing ... -out $filename -sc $sc -cp $cp -nn $numberOfNodes -locs $locs -x $x -y $y -stop $overallTime -mac_emu $mac_emu\n";
	system("nohup ns $run_tcl -out $filename -sc $sc -cp $cp -nn $numberOfNodes -locs $locs -x $x -y $y -stop $overallTime -zip 1 -use_peri $use_peri -mac_emu $mac_emu");
    }
    chdir "..";
}


sub parseCmdLine {
  for (my $i = 0; $i <= $#ARGV; $i++) {
      if ($ARGV[$i] eq "-r") {
	  $recursive = "true";
      }
      if ($ARGV[$i] eq "-gls") {
	  $locs = "2";
      }
      if ($ARGV[$i] eq "-no_peri") {
	  $use_peri = "0";
      }
      if ($ARGV[$i] eq "-help") {
	  printHelp();
	  exit;
      }  

      if ($ARGV[$i] eq "-queries") {
	  $necessary_queries = $ARGV[$i+1];
	  $i++;
      }   

      if ($ARGV[$i] eq "-run_tcl") {
	  $run_tcl = $ARGV[$i+1];
	  $i++;
      }    

      if ($ARGV[$i] eq "-no_mac_emu") {
	  $mac_emu = "0";
      }         
  }
}


sub printHelp {

    print "This script is designed to run an ns simulation from an\n";
    print "existing directory\n";
    print "Options:\n";
    print "  -r (recursive)                      DEFAULT : false\n";
    print "  -gls (use GLS)                      DEFAULT : HLS\n";
    print "  -no_peri (don't use perimeter mode) DEFAULT : use it\n";
    print "  -queries (nr of queries which must be in the evaluated file to\n";
    print "            avoid running the sim     DEFAULT : 1200\n";
    print "  -run_tcl <full path to run.tcl>     DEFAULT : /home/stud/kiess/space/ns-util/run.tcl\n";
    print "  -no_mac_emu (no mac emulation)      DEFAULT : mac emulation\n";
    print "  -help (show this help)\n";
    print "\n";
}
