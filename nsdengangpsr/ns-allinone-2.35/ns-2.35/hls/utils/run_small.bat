#!/bin/bash

############################
# configuration
############################
nn="100"
speed="30"
x="2000"
y="2000"
stop="100"
trafficStop=`expr $stop - 2`
startup="15"
mode="0" 
# test-query, ...
MRD="1"
mrdString="-MRD"
runs="1"
active="100"
connectionsPerNode="4"
totalConnections=`expr $active \* $connectionsPerNode`

peri="1"
macemu="0"
#reactive="0"


cp="cp-n$nn-c$totalConnections-$startup-$trafficStop.tcl"
sc="sc-x$x-y$y-n$nn-s$speed-t$stop$mrdString"



# local variables
counter="0"
while [ $counter -lt $runs ]
do
  echo "Run Configuration: -n $nn -s $speed -t $stop -x $x -y $y -mrd $MRD -c $totalConnections"
  counter=$((counter+1))
  date=$(date +%j:%T)
  
  directory="$nn-$speed-$date";
  #./scengen -n $nn -s $speed -t $stop -x $x -y $y -m $MRD > $sc
  #./trafgen -n $nn -a $active -t $trafficStop -c $connectionsPerNode -m $mode -w $startup > $cp
  

  sc="sc-x2000-y2000-n100-s30-t100-MRD"
  cp="cp-n100-c400-15-98.tcl"
  
  perl ./namable.pl $sc $sc
  
  mkdir $directory
  # HLS
  #ns run.tcl -out $directory/hls_trace.txt -sc $sc -cp $cp -nn $nn -locs 3 -use_peri 0 -x $x -y $y -mac_emu $macemu -stop $stop -zip 1 
  # GLS
  #ns run.tcl -out $directory/gls_trace.txt -sc $sc -cp $cp -nn $nn -locs 2 -use_peri $peri -x $x -y $y -mac_emu $macemu -stop $stop -zip 1 
  # HLS with peri
  ../../ns run.tcl -out $directory/hls_trace_peri.txt -sc $sc -cp $cp -nn $nn -locs 3 -use_peri 1 -x $x -y $y -mac_emu $macemu -stop $stop #-zip 1
# end of simulation, now copying the input files
  cp $sc $directory
  cp $cp $directory
  cd $directory
  #evaluate.pl -f hls_trace.txt.gz > evaluated
  #gls_evaluate.pl -f gls_trace.txt.gz > gls_evaluated
  perl ../evaluate.pl -f hls_trace_peri.txt > peri_evaluated
  cd ..
done




	
#cd $directory
#evaluate.pl -f hls_trace.txt.gz > evaluated
