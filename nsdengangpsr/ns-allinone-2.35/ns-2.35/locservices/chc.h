#/*

  Contacts: 
  Michael Kaesemann (mikael@uni-mannheim.de)

  Permission to use, copy, modify, and distribute this software and its
  documentation in source and binary forms is hereby granted, provided 
  that the above copyright notice appears in all copies and the author is 
  acknowledged in all documentation pertaining to any such copy or
  derivative work.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*/

#ifndef _CHC_h
#define _CHC_h

#include "hdr_locs.h"

#define CHC_BASE_SIZE  97 /* Good prime for simple hash              */
#define PRINT_ONE_LINE    /* Print output of TableStats as One-Liner */

/**************/
/* Base Class */
/**************/

class ChainedHashCache {

 public:
  ChainedHashCache(
		   class LocationService* parent, 
		   const unsigned int hash_size = CHC_BASE_SIZE, 
		   const double lifetime = 0.0 )
    {
      this->parent = parent;
      this->size = hash_size;
      if (lifetime < 0.0) { this->lifetime = 0.0;      }
      else                { this->lifetime = lifetime; }
	    
      table = new CHCEntry*[hash_size];
	    
      for (unsigned int i=0; i<hash_size; i++) {
	table[i] = NULL;
      }
    }

  virtual ~ChainedHashCache() {
    for (unsigned int i=0; i < size; i++) {
      while (table[i] != NULL) {
	CHCEntry* tmp = table[i]->next;
	table[i]->next->prev = NULL;
	delete table[i];
	table[i] = tmp;
      }
    }
    delete[] table;
  }

  bool add(unsigned int ikey, void* entry);
  bool update(unsigned int ikey, void* entry);
  void* search(unsigned int ikey);
  void remove(unsigned int ikey);
    
  // Unfortunately GLS needs access to all entries for its closest
  // next hop scheme :( Maybe I'll find another way later - mk
  CHCEntry** getTable(unsigned int *size) {
    for (unsigned int i=0; i < this->size; i++) { cleanUp(i); }
    (*size) = this->size;
    return table;
  }

  void printTable();
  void printTableStats(const int id = -1);
  void tableStats(double *avg_, int* max_);

 protected:
  CHCEntry** table;
  unsigned int size;
  double lifetime;
  class LocationService* parent;

  CHCEntry* searchCHC(unsigned int ikey);
  CHCEntry* removeCHC(CHCEntry* victim);
  void cleanUp(const unsigned int ikey);

  // Information handlers; Need to be overwritten
  virtual bool iupdate(void*, void*) { return false; }
  virtual bool itimeout(void*) { return false; }
  virtual void* inew () = 0; 
  virtual void idelete(void*) = 0;
  virtual void icopy(void*, void*) = 0;
  virtual void iprint(void*) = 0;
};

/*****************/
/* Child Classes */
/*****************/

class LSLocationCache : public ChainedHashCache {
 public:
  LSLocationCache(class LocationService* parent, const unsigned int hash_size = CHC_BASE_SIZE, const double lifetime = 0.0 ) 
    : ChainedHashCache(parent,hash_size,lifetime) {}
  ~LSLocationCache() {}

  bool add(nodelocation*);
  bool update(nodelocation*);

 private:
  bool iupdate (void*, void*);
  bool itimeout (void*);
  void iprint (void*);
  inline void* inew() { return ((void*)(new nodelocation)); }
  inline void idelete (void* ventry) { delete (nodelocation*)ventry; }
  inline void icopy (void* ventry, void* vdata) { memcpy(ventry,vdata,sizeof(nodelocation)); }
};

class LSRequestCache : public ChainedHashCache {
 public:
  LSRequestCache(class LocationService* parent, const unsigned int hash_size = CHC_BASE_SIZE, const double lifetime = 0.0 ) 
    : ChainedHashCache(parent,hash_size,lifetime) {}
  ~LSRequestCache() {}

  bool add(locrequest*);
  bool update(locrequest*);

 private:
  bool iupdate (void*, void*);
  bool itimeout (void*);
  void iprint (void*);

  inline void* inew() { return ((void*)(new locrequest)); }
  inline void idelete (void* ventry) { delete (locrequest*)ventry; }
  inline void icopy (void* ventry, void* vdata) { memcpy(ventry,vdata,sizeof(locrequest)); }
};

class LSSeqNoCache : public ChainedHashCache {
 public:
  LSSeqNoCache(class LocationService* parent, const unsigned int hash_size = CHC_BASE_SIZE, const double lifetime = 0.0 ) 
    : ChainedHashCache(parent,hash_size,lifetime) {}
  ~LSSeqNoCache() {}

  bool add(seqnoentry*);
  bool update(seqnoentry*);
  const int find(const unsigned int);

 private:
  bool iupdate (void*, void*);
  bool itimeout (void*);
  void iprint (void*);

  inline void* inew() { return ((void*)(new seqnoentry)); }
  inline void idelete (void* ventry) { delete (seqnoentry*)ventry; }
  inline void icopy (void* ventry, void* vdata) { memcpy(ventry,vdata,sizeof(seqnoentry)); }
};

class LSSupCache : public ChainedHashCache {
 public:
  LSSupCache(class LocationService* parent, 
	     const unsigned int hash_size = CHC_BASE_SIZE, 
	     const double lifetime = 0.0 ) 
    : ChainedHashCache(parent,hash_size,lifetime) {}
  ~LSSupCache() {}

  bool add(supinfo*);
  bool update(supinfo*);

 private:
  bool iupdate (void*, void*);
  bool itimeout (void*);
  void iprint (void*);

  inline void* inew() { return ((void*)(new supinfo)); }
  inline void idelete (void* ventry) { delete (supinfo*)ventry; }
  inline void icopy (void* ventry, void* vdata) { memcpy(ventry,vdata,sizeof(supinfo)); }
};

#endif //_CHC_h
