/*

  Contacts: 
  Michael Kaesemann (mikael@uni-mannheim.de)

  Permission to use, copy, modify, and distribute this software and its
  documentation in source and binary forms is hereby granted, provided 
  that the above copyright notice appears in all copies and the author is 
  acknowledged in all documentation pertaining to any such copy or
  derivative work.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*/

#ifndef _LS_Queued_Timer_h
#define _LS_Queued_Timer_h

#include <timer-handler.h>
#include <packet.h>

/*****************/
/* Queued Timer */
/*****************/

class QueuedTimer : public TimerHandler {

 public:
    QueuedTimer(int size);
    QueuedTimer(int size, bool mwarn);
    virtual ~QueuedTimer();
    virtual void handle() = 0;
    virtual void deleteInfo(void* info) {}
    void notify();
    bool remove(int32_t key_);
    bool remove(unsigned int key_);
    bool queued(unsigned int key_);
    void* const peek(unsigned int key_);
    void* extract(unsigned int key_, double* ttime_ = NULL);
    void shutdown();
    void add(int32_t key_, double delay_, void* info_ = NULL);
    void add(unsigned int key_, double delay_, void* info_ = NULL);
    void expire(Event *e);
    unsigned int countEntries(unsigned int key_);
    void printQueue();

    void setParent(int id) { parent = id; }

 protected:
    class TimerHeap *heap_;
    struct TimerHeapEntry *in_progress;
    unsigned int local_key;
    void* local_info;
    int parent;
    bool warn_multiple;
};

#endif //_LS_Queued_Timer_h






