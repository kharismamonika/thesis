/*

  Contacts: 
  Michael Kaesemann (mikael@uni-mannheim.de)

  Permission to use, copy, modify, and distribute this software and its
  documentation in source and binary forms is hereby granted, provided 
  that the above copyright notice appears in all copies and the author is 
  acknowledged in all documentation pertaining to any such copy or
  derivative work.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*/

#ifndef _CHC_Caches_cc
#define _CHC_Caches_cc

#include "chc.h"

/******************/
/* Location Cache */
/******************/

bool LSLocationCache::add(nodelocation* info) {
    return (ChainedHashCache::add(info->id, (void*)(info)));
}

bool LSLocationCache::update(nodelocation* info) {
    return (ChainedHashCache::update(info->id, (void*)(info)));
}

bool LSLocationCache::iupdate (void* ventry, void* vdata) {
    if (((nodelocation*)ventry)->ts < ((nodelocation*)vdata)->ts) {
	memcpy(ventry,vdata,sizeof(nodelocation));
	return true;
    }
    return false;
}

bool LSLocationCache::itimeout (void* ventry) {
    if (lifetime == 0) {
	if (((((nodelocation*)ventry)->timeout) > 0.0) && 
	    (Scheduler::instance().clock() > ((nodelocation*)ventry)->timeout))
	{ return true; }
    }else {
	if ((Scheduler::instance().clock() - ((nodelocation*)ventry)->ts) > lifetime) 
	{ return true; }
	
    }
    return false;
}

void LSLocationCache::iprint (void* ventry) {
    nodelocation* entry = (nodelocation*)ventry;
    printf("(%d,%.2f/%.2f,%d:%d %.4f,%.4f)",entry->id,entry->loc.x,entry->loc.y,entry->sqr.grid,entry->sqr.order,entry->ts,entry->timeout);
}

/*****************/
/* Request Cache */
/*****************/

bool LSRequestCache::add(locrequest* info) {
    return (ChainedHashCache::add(info->dst, (void*)(info)));
}

bool LSRequestCache::update(locrequest* info) {
    return (ChainedHashCache::update(info->dst, (void*)(info)));
}

bool LSRequestCache::iupdate (void* ventry, void* vdata) {
    memcpy(ventry,vdata,sizeof(locrequest));
    return true;
}

bool LSRequestCache::itimeout (void* ventry) {
    if (lifetime == 0) { return false; }
    if ((Scheduler::instance().clock() - ((locrequest*)ventry)->ts) > lifetime) 
    { return true; }
    return false;
}

void LSRequestCache::iprint (void* ventry) {
    locrequest* entry = (locrequest*)ventry;
    printf("(%d,%d,%d %.4f)",entry->dst,entry->seqno,entry->maxhop, entry->ts);
}

/***************/
/* SeqNo Cache */
/***************/

bool LSSeqNoCache::add(seqnoentry* info) {
    return (ChainedHashCache::add(info->key, (void*)(info)));
}

bool LSSeqNoCache::update(seqnoentry* info) {
    return (ChainedHashCache::update(info->key, (void*)(info)));
}

const int LSSeqNoCache::find(const unsigned int key) {
    void *tmp = ChainedHashCache::search(key);
    if (tmp != NULL) {
	return (((seqnoentry*)tmp)->seqno);
    }else{ return (-1); }
}

bool LSSeqNoCache::iupdate (void* ventry, void* vdata) {
    memcpy(ventry,vdata,sizeof(seqnoentry));
    return true;
}

bool LSSeqNoCache::itimeout (void* ventry) {
    if (lifetime == 0) { return false; }
    if ((Scheduler::instance().clock() - ((seqnoentry*)ventry)->ts) > lifetime) 
    { return true; }
    return false;
}

void LSSeqNoCache::iprint (void* ventry) {
    seqnoentry* entry = (seqnoentry*)ventry;
    printf("(%d,%d, %.4f)",entry->key, entry->seqno, entry->ts);
}

/*********************/
/* Suppression Cache */
/*********************/

bool LSSupCache::add(supinfo* info) {
    return (ChainedHashCache::add(info->key, (void*)(info)));
}

bool LSSupCache::update(supinfo* info) {
    return (ChainedHashCache::update(info->key, (void*)(info)));
}

bool LSSupCache::iupdate (void* ventry, void* vdata) {
    memcpy(ventry,vdata,sizeof(supinfo));
    return true;
}

bool LSSupCache::itimeout (void* ventry) {
    if (lifetime == 0) { return false; }
    if ((Scheduler::instance().clock() - ((supinfo*)ventry)->ts) > lifetime) 
    { return true; }
    return false;
}

void LSSupCache::iprint (void* ventry) {
    supinfo* entry = (supinfo*)ventry;
    printf("(%d,%d,%.2f,%d %.4f)",entry->key, entry->cnt, entry->dmin, (int)entry->blocked, entry->ts);
}

#endif
