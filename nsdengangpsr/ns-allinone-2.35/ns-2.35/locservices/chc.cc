/*

  Contacts: 
  Michael Kaesemann (mikael@uni-mannheim.de)

  Permission to use, copy, modify, and distribute this software and its
  documentation in source and binary forms is hereby granted, provided 
  that the above copyright notice appears in all copies and the author is 
  acknowledged in all documentation pertaining to any such copy or
  derivative work.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*/

#ifndef _CHC_cc
#define _CHC_cc

#include "chc.h"

/*****************************/
/* Base : Chained Hash Cache */
/*****************************/

//! updates an entry if necessary
bool ChainedHashCache::update(unsigned int ikey, void* entry) {
    
    const unsigned int key = (ikey % size); 

    cleanUp(key);

    CHCEntry* last = table[key];
    CHCEntry* tmp = table[key];

    // Walk the Hash Chain and try to find an existing entry
    while (tmp != NULL) {
	if (tmp->key == ikey) {
	    // Found entry
	    iupdate(tmp->info,entry);
	    return true;
	}else{
	    last = tmp;
	    tmp = tmp->next;
	}
    }
    return false;
}

//! adds a new entry or updates an old one   
bool ChainedHashCache::add(unsigned int ikey, void* entry) {

    const unsigned int key = (ikey % size); 

    cleanUp(key);

    CHCEntry* last = table[key];
    CHCEntry* tmp = table[key];

    // Walk the Hash Chain and try to find an existing entry
    while (tmp != NULL) {
	if (tmp->key == ikey) {
	    // Found entry
	    iupdate(tmp->info,entry);
	    return false;
	}else{
	    last = tmp;
	    tmp = tmp->next;
	}
    }

    tmp = new CHCEntry;
    tmp->key = ikey;

    void* tinfo = inew();
    if (tinfo == NULL) { 
	printf("Warning: Could not generate CacheEntry !\n"); 
	abort(); 
    }
    icopy(tinfo,entry);
    tmp->info = tinfo;

    // Correct list linkage
    tmp->next = NULL;
    tmp->prev = last;
    if (last != NULL) { last->next = tmp; }

    if (table[key] == NULL) { table[key] = tmp; }

    return true;
}

//! removes an entry and returns the next element or NULL
CHCEntry* ChainedHashCache::removeCHC(CHCEntry* victim) {

    assert(victim != NULL);
    CHCEntry* tmp = victim->next;

    unsigned int key = (victim->key % size); 

    // Correct successor
    if (victim->next != NULL) { victim->next->prev = victim->prev; }
    // Correct predecessor
    if (victim->prev != NULL) { victim->prev->next = victim->next; }
    // Correct Chain Head
    if (table[key] == victim) { table[key] = victim->next; }

    idelete (victim->info);
    delete victim;
    return tmp;
}

//! removes timed out entries from the selected hash chain
void ChainedHashCache::cleanUp(const unsigned int ikey) {

    CHCEntry* tmp = table[ikey];

    while (tmp != NULL) {
	if (itimeout(tmp->info)) {
	    tmp = removeCHC(tmp);
	}else{
	    tmp = tmp->next;
	}
    }
    return;
}

//! searches the cache for an entry that fits the key
void* ChainedHashCache::search(unsigned int ikey) {

    unsigned int key = (ikey % size); 

    cleanUp(key);

    CHCEntry* last = table[key];
    CHCEntry* tmp = table[key];
    while (tmp != NULL) {
	if (tmp->key == ikey) {
	    return tmp->info;
	    break;
	} else {
	    last = tmp;
	    tmp = tmp->next;
	}
    }
    return NULL;
}

CHCEntry* ChainedHashCache::searchCHC(unsigned int ikey) {

    unsigned int key = (ikey % size); 

    cleanUp(key);

    CHCEntry* last = table[key];
    CHCEntry* tmp = table[key];
    while (tmp != NULL) {
	if (tmp->key == ikey) {
	    return tmp;
	    break;
	} else {
	    last = tmp;
	    tmp = tmp->next;
	}
    }
    return NULL;
}

//! searches the cache for an entry that fits the key
void ChainedHashCache::remove(const unsigned int ikey) {

    // Delete Entry
    CHCEntry* tmp = searchCHC(ikey);
    if (tmp != NULL) { removeCHC(tmp); }
}

//! print the table for debugging
void ChainedHashCache::printTable() {

    for (unsigned int i=0; i<size; i++) {
	if (table[i] != NULL) {
	    CHCEntry* tmp = table[i];
	    printf("Row %d: ",i);
	    while (tmp != NULL) {
		iprint(tmp->info);
		printf(" ");
		tmp = tmp->next;
	    }
	    printf("\n");
	}else{
	    continue;
	}
    }   
}

//! print the table statistics for debugging
void ChainedHashCache::printTableStats(const int id) {

    double avg;
    int max;

    tableStats(&avg,&max);
#ifndef PRINT_ONE_LINE
    if (id != -1) {
	printf("LocationCache Statistics for %d:\n",id);
	printf("-------------------------------\n");
    }else{
	printf("LocationCache Statistics:\n");
	printf("------------------------\n");
    }
    printf("Max. Line Usage       : %d\n",max);
    printf("Average Line Usage    : %.4f\n",avg);
#else
    if (id != -1) {
	printf("LocationCache Statistics for %d: %d\t%.4f\t\t\n",id,max,avg);
    }else{
	printf("LocationCache Statistics: %d\t%.4f\t\t\n",max,avg);
    }
#endif
}

//! evaluates some satistics for this CHC
void ChainedHashCache::tableStats(double *avg_, int* max_) {

    int avg = 0;
    int max = 0;
    int line = 0;
    
    for (unsigned int i=0; i<size; i++) {
	if (table[i] != NULL) {
	    cleanUp(i);
	    line = 0;
	    CHCEntry* tmp = table[i];
	    while (tmp != NULL) {
		avg++;
		line++;
		tmp = tmp->next;
	    }
	    if (line > max) { max = line; }
	}else{
	    continue;
	}
    }

    if (max_ != NULL) { *max_ = max; }
    if (avg_ != NULL) { *avg_ = avg/size; }
}

#endif //_CHC_cc
