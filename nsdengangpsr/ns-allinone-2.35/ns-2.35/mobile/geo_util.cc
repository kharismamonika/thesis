/*
  This is a collection of functions that were globally defined in
  multiple files.
*/
using namespace std;

#include "geo_util.h"
#include <math.h>
//#include <vector>
//#define DEBUG
/** calculates the distance in 3 Dimensions */
double distance(double x1, double y1, double z1, double x2, double y2,
		       double z2)
{
  double dist = sqrt((x1 - x2)*(x1 - x2) +
		     (y1 - y2)*(y1 - y2) +
		     (z1 - z2)*(z1 - z2));
  return dist;
}

/** calculates the bearing (in radian) */
double bearing(double x1, double y1, double x2, double y2)
{
  double brg;

  // XXX only deal with 2D for now
  // XXX check for (0, 0) args, a domain error
  brg = atan2(y2 - y1, x2 - x1);
  while (brg < 0)
    brg += 2*M_PI;
  return brg;
}

int cross_segment(double x1, double y1, double x2, double y2,
						 double x3, double y3, double x4, double y4,
						 double *xi /*= 0*/, double *yi /*= 0*/)
{
	double dy[2], dx[2], m[2], b[2];
	double xint, yint;
/*	#ifdef DEBUG
    FILE *fp;
    fp = fopen ("debug.txt", "a+");
    fprintf (fp, "\n%s functionx, line %d, %f, %f, %f, %f, %f, %f, %f, %f\n", __FUNCTION__, __LINE__,
      x1, y1, x2, y2, x3, y3, x4, y4);
    fclose (fp);
  #endif*/
	dy[0] = y2 - y1; // dsty - pty
	dx[0] = x2 - x1; // dstx - ptx
	dy[1] = y4 - y3; // ne->y - myy
	dx[1] = x4 - x3; // ne->x - myx
  if(dx[0]==0 || dx[1]==0) return 0;
	m[0] = dy[0] / dx[0];
	m[1] = dy[1] / dx[1];
	b[0] = y1 - m[0] * x1;
	b[1] = y3 - m[1] * x3;
	if (m[0] != m[1]) {
		// slopes not equal, compute intercept
		xint = (b[0] - b[1]) / (m[1] - m[0]);
		yint = m[1] * xint + b[1];
		// is intercept in both line segments?
		if ((xint <= maxd(x1, x2)) && (xint >= mind(x1, x2)) &&
			(yint <= maxd(y1, y2)) && (yint >= mind(y1, y2)) &&
			(xint <= maxd(x3, x4)) && (xint >= mind(x3, x4)) &&
			(yint <= maxd(y3, y4)) && (yint >= mind(y3, y4))) {
			if (xi && yi) {
				*xi = xint;
				*yi = yint;
			}
			return 1;
		}
	}
	return 0;
}

double norm(double tmp_bear){
    float to_norm = tmp_bear;
    if(to_norm < -1000.0)
      to_norm = 0.0;
    while(to_norm <= 0)
	to_norm += 2*M_PI;
    if(to_norm > 1000.0)
      to_norm = 0.0;
    while(to_norm > 2*M_PI)
	to_norm -= 2*M_PI;
    
    return (double)to_norm;
}

/** returns x with a_i + b_i * x = a_j + b_j * x */
bool cut(double a_i, double b_i, double a_j, double b_j, double *x, double *y){
  /*
    a_i + b_i * x = a_j + b_j * x
    x = (a_j - a_i) / (b_i - b_j)
  */
    
  if(b_i == b_j)
    return false;

  *x = (a_j - a_i) / (b_i - b_j);
  *y = a_j + b_j * *x;
  return true;

}

/**
 * here are some statistical functions 
 * which are student-t distribution, incomplete beta function and gammaln function
 */

/** gammaln returns ln(\Gamma(xx)) */
double gammln(double xx){
    double x,y,tmp,ser;
    
    static double cof[6]={76.18009172947146,-86.50532032941677,
			  24.01409824083091,-1.231739572450155,
			  0.1208650973866179e-2,-0.5395239384953e-5};

    y = x = xx;
    tmp = x + 5.5;
    tmp -= (x+0.5)*log(tmp);
    ser = 1.000000000190015;

    for (int j=0; j<=5; j++)
	ser += cof[j] / ++y;

    return -tmp+log(2.5066282746310005 * ser / x);
}


/** Used by betai: Evaluates continued fraction for incomplete beta function by modi ed Lentz's method (x5.2). */
double betacf(double a, double b, double x){
    /* 228 Chapter 6. Special Functions*/ 

    int m, m2;
    double aa, c, d, del, h, qab, qam, qap;

    /* These q's will be used in factors that occur in the coe cients (6.4.6).*/
    qab = a + b;
    qap = a + 1.0;
    qam = a - 1.0;

    /* First step of Lentz's method.*/
    c = 1.0;
    d = 1.0 - qab * x / qap;
    if (fabs(d) < FPMIN)
	d=FPMIN;
    d = 1.0 / d;
    h = d;
    for (m=1; m<=MAXIT; m++) {
	m2 = 2 * m;
	aa = m * (b-m)*x / ((qam+m2) * (a+m2));

        /* One step (the even one) of the recurrence.*/
	d = 1.0 + aa * d;
	if (fabs(d) < FPMIN)
	    d=FPMIN;
	c = 1.0 + aa / c;
	if (fabs(c) < FPMIN)
	    c = FPMIN;
	d = 1.0 / d;
	h *= d*c;
	aa = -(a+m)*(qab+m)*x / ((a+m2)*(qap+m2));

        /* Next step of the recurrence (the odd one).*/
	d = 1.0 + aa * d;
	if (fabs(d) < FPMIN)
	    d=FPMIN;
	c = 1.0 + aa / c;
	if (fabs(c) < FPMIN)
	    c = FPMIN;
	d = 1.0 / d;
	del = d * c;
	h *= del;

	/* Are we done? */
	if (fabs(del-1.0) < EPS)
	    break;
    }
 
    if (m > MAXIT) 
	nrerror("a or b too big, or MAXIT too small in betacf");

    return h;
}



/** Returns the incomplete beta function Ix(a; b). */
double betai(double a, double b, double x)
{
    double bt;
    if (x < 0.0 || x > 1.0)
	nrerror("Bad x in routine betai");

    if (x == 0.0 || x == 1.0){
	bt = 0.0;
    }else{
	/*Factors in front of the continued fraction.*/
	bt = exp(gammln(a+b) - gammln(a) - gammln(b) + a*log(x) + b*log(1.0-x));
    }

    /* Use continued fraction directly.*/
    if (x < (a+1.0) / (a+b+2.0)){
	return bt * betacf(a,b,x) / a;
    }else{ 
	/* Use continued fraction after making the symmetry transformation.*/
	return 1.0 - bt * betacf(b,a,1.0-x) / b;
    }
}

/** return student-t's quantil */
double student(double t, double nu){
    return 1 - betai(nu/2.0, 0.5, nu/(nu+pow(t,2)));
}

//float select(unsigned long k, unsigned long n, vector<float>& arr){
float select(unsigned long k, unsigned long n, float *arr){
  unsigned long i,ir,j,l,mid;
  float a,temp;
  i=0;
  l=1;
  ir=n;
  for (;;){
    if (ir <= l+1){
      /* Active partition contains 1 or 2e lements. */
      if (ir == l+1 && arr[ir] < arr[l]){
	/* Case of 2el ements. */
	swap(&arr[l],&arr[ir]);
      }
      temp = arr[k];
      return temp;
    }else{
      mid=(l+ir) >> 1;
      /* Choose median of left, center, and right elements as partitioning element a. Also rearrange so that arr[l] d arr[l+1], arr[ir] e arr[l+1]. */
      swap(&arr[mid],&arr[l+1]);
      if (arr[l] > arr[ir]){
	swap(&arr[l],&arr[ir]);
      }
      if (arr[l+1] > arr[ir]){
	swap(&arr[l+1],&arr[ir]);
      }
      if (arr[l] > arr[l+1]){
	swap(&arr[l],&arr[l+1]);
      }
      i=l+1;
      /* Initialize pointers for partitioning. */
      j=ir;
      a=arr[l+1];
      /* Partitioning element. */

      for (;;){
	/* Beginning of innermost loop. */
	do i++;
	while (arr[i] < a);
	/* Scan up to  nd element > a. */
	do j--;
	while (arr[j] > a);
	/* Scan down to  nd element < a. */
	if (j < i)
	  break;
	/* Pointers crossed. Partitioning complete. */
	swap(&arr[i],&arr[j]);
      }
      /* End of innermost loop. */
      arr[l+1]=arr[j];
      /* Insert partitioning element. */
      arr[j]=a;
      if (j >= k)
	ir=j-1;
      /* Keep active the partition that contains the kth element. */
      if (j <= k)
	l=i;
    }
  }
}




// /************************************************************************
//  * C code
//  ***********************************************************************/


// /** allocate a float vector with subscript range v[nl..nh] */ 
// float *vector(long nl, long nh){
//   float *v;
//   v=(float *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(float)));
//   if (!v) nrerror("allocation failure in vector()");
//   return v-nl+NR_END;
// }

// /** free a float vector allocated with vector() */ 
// void free_vector(float *v, long nl, long nh){
//   free((FREE_ARG) (v+nl-NR_END));
// }

