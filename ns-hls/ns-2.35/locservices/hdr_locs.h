/*

  Contacts: 
  Michael Kaesemann (mikael@uni-mannheim.de)

  Permission to use, copy, modify, and distribute this software and its
  documentation in source and binary forms is hereby granted, provided 
  that the above copyright notice appears in all copies and the author is 
  acknowledged in all documentation pertaining to any such copy or
  derivative work.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*/

#ifndef _hdr_locs_h
#define _hdr_locs_h

#include <packet.h>

#ifndef NO_NODE
#define NO_NODE -2
#endif

// Simple Location Service
#define LOCS_REQUEST    1
#define LOCS_REPLY      2
#define LOCS_DATA       3
#define LOCS_UPDATE     4
#define LOCS_UPDATE_ACK 5
#define LOCS_NOTIFY     6

// TimerHeap Element
typedef struct TimerHeapEntry{
    unsigned int key;
    double ts;
    void* info;
};

typedef struct position {
    double x, y, z;
};

typedef struct square {
    int grid;
    unsigned int order;
};

typedef struct CHCEntry {
    unsigned int key;
    CHCEntry* next;
    CHCEntry* prev;

    // Payload
    void* info;
};

typedef struct nodelocation {
    nsaddr_t id;
    double ts;
    double timeout;
    position loc;
    square sqr;
};

typedef struct locrequest {
    nsaddr_t dst;
    double ts;
    unsigned int maxhop;
    unsigned int seqno;
};

typedef struct seqnoentry {
    unsigned int key;
    double ts;
    unsigned int seqno;
};

typedef struct supinfo {
    unsigned int key;
    double ts;
    unsigned int cnt;
    double dmin;
    bool blocked;
};

typedef struct updcacheentry {
    unsigned int cnt;
    square sqr;
};

typedef struct connectionentry {
    nsaddr_t dst;
    double ts;
    connectionentry* next;
    connectionentry* prev;
};

struct hdr_locs {

    nodelocation src;
    nodelocation dst;
    nodelocation lasthop;
    nodelocation next;

    unsigned int valid_;
    int type_;
    unsigned int seqno_;
    unsigned int maxhop_;
    square upddst_;
    bool in_correct_grid;
    const char* updreason_;
    int callback_;

    inline void init() {	
	src.id = NO_NODE;
	src.ts = -1.0;
	src.timeout = -1.0;
	src.loc.x = -1.0;
	src.loc.y = -1.0;
	src.loc.z = -1.0;
	src.sqr.grid = -1;
	src.sqr.order = 0;

	dst.id = NO_NODE;
	dst.ts = -1.0;
	dst.timeout = -1.0;
	dst.loc.x = -1.0;
	dst.loc.y = -1.0;
	dst.loc.z = -1.0;
	dst.sqr.grid = -1;
	dst.sqr.order = 0;

	next.id = NO_NODE;
	next.ts = -1.0;
	next.timeout = -1.0;
	next.loc.x = -1.0;
	next.loc.y = -1.0;
	next.loc.z = -1.0;
	next.sqr.grid = -1;
	next.sqr.order = 0;

	lasthop.id = NO_NODE;
	lasthop.ts = -1.0;
	lasthop.timeout = -1.0;
	lasthop.loc.x = -1.0;
	lasthop.loc.y = -1.0;
	lasthop.loc.z = -1.0;
	lasthop.sqr.grid = -1;
	lasthop.sqr.order = 0;

	upddst_.grid = -1;
	upddst_.order = 0;

	valid_ = 1;
	type_ = 0;
	maxhop_ = 0;
	seqno_ = 0;
	in_correct_grid = false;
	updreason_ = "NRSN";
	callback_ = 0;
    } 

    // NS Rudimentary
    static int offset_;
    inline static int& offset() { return offset_; } 
    inline static hdr_locs* access(const Packet* p) {
	return (hdr_locs*) p->access(offset_);
    }
    
    int size(bool implicit_beacon = false) {
	
	return 0;
    }
};  

#endif // _hdr_locs_h




