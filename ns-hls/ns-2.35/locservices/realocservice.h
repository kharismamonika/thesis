/*

  Contacts: 
  Michael Kaesemann (mikael@uni-mannheim.de)

  Permission to use, copy, modify, and distribute this software and its
  documentation in source and binary forms is hereby granted, provided 
  that the above copyright notice appears in all copies and the author is 
  acknowledged in all documentation pertaining to any such copy or
  derivative work.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*/

#ifndef _ReaLocService_h
#define _ReaLocService_h

#include "locservice.h"
#include "ls_queued_timer.h"
#include <random.h>

// NOTE: Sizes are prime numbers, because they are 
//       implemented as Chained Hashes - mk
#define RLS_TTL                   64
#define RLS_LOCCACHE_TIMEOUT      5.0
#define RLS_LOCCACHE_SIZE         97
#define RLS_REQ_MAXHOP            32
#define RLS_REQ_INITHOP           2
#define RLS_REQ_LINSTEPHOP        2
#define RLS_REQTABLE_HOP_TIMEOUT  0.030
#define RLS_REQTABLE_TIMEOUT      (RLS_REQ_MAXHOP * RLS_REQTABLE_HOP_TIMEOUT)
#define RLS_REQTABLE_SIZE         199
#define RLS_FORWCACHE_TIMEOUT     10.0
#define RLS_FORWCACHE_SIZE        97

// This one is not a Hash, so no prime number is needed
#define RLS_HEAP_SIZE             32

#define RLS_IMMEDIATE_NOTIFICATION true

// Mix x & y into one unique key (at least for a reasonable number of nodes, i.e 0.5*2^sizeof(int))
#define key(x,y)  (((x)<<16)+(y)) 

// Max Request Broadcast Delay
#define RLS_MAX_BCAST_DELAY    0.015 /* 0-15 ms Delay; inactive if DF is active */

#define ALLOW_EXP_REQUEST
#define ALLOW_MAX_FLOOD
#undef  ALLOW_CACHED_REPLY
#undef  ALLOW_MULTIPLE_REPLIES

// Request Suppression
#undef ALLOW_REQUEST_SUPPRESSION      
#define RLS_REQDELAY_SIZE        8    /* Initial delay queue size       */
#define RLS_SUPRAD_TIME          0.02 /* RAD of broadcasts              */
#define RLS_SUPINFOCACHE_TIMEOUT 20   /* Wait no longer than 20 secs    */
                                      /* for rebroadcasts of a pkt      */
#define RLS_SUPINFOCACHE_SIZE    97
#define RLS_DIST_THRESHOLD       45   /* nodes closer than this         */
			              /* should not rebroadcast         */
#define RLS_CNT_THRESHOLD        4    /* pkts heard more often than     */
                                      /* this should not be rebroadcast */
// Directional Flooding
#define ALLOW_DIRECTIONAL_FLOODING
#define RLS_DF_MAX_DELAY  0.020   /* 0.0375 Max Delay for Req Forwarding   */

// Debugging
#define locs_verbose          true
#define locs_show_processing  false
#define locs_show_in_progress false

class ReaLocService;

class RLSRequestScheduler : public QueuedTimer {
    
 public:
    RLSRequestScheduler(class ReaLocService *ls, int size)
	: QueuedTimer(size) 
	{ ls_ = ls; }
    void handle();

 private:
    ReaLocService* ls_; 

};

class RLSRequestDelay : public QueuedTimer {
    
 public:
    RLSRequestDelay(class ReaLocService *ls, int size)
	: QueuedTimer(size) 
	{ ls_ = ls; }
    void handle();
    void deleteInfo(void* info);

 private:
    ReaLocService* ls_; 

};

class ReaLocService : public LocationService {

 public:
    ReaLocService(class Agent *p);
    ~ReaLocService();
    void recv(Packet* &p);
    bool poslookup(Packet *p);
    void evaluatePacket(const Packet *p);
    void sleep();
    int hdr_size(Packet* p);
    void callback(Packet* &p);

 protected:
    void nextRequestCycle(nsaddr_t dst_);
    void forwardRequest(Packet* &p);

 private:

    class LSRequestCache *reqtable;
    class LSLocationCache *loccache;
    class LSSeqNoCache *forwcache;
    class LSSeqNoCache *seqnocache;

    class RLSRequestScheduler *reqtimer;
    //friend class GPSR_Agent;
    friend class PBRAgent;
    friend class RLSRequestScheduler;
    friend class RLSRequestDelay;

#ifdef ALLOW_REQUEST_SUPPRESSION
    class RLSRequestDelay *reqdelay;
    class LSSupCache *supinfocache;
#endif

    void recvRequest(Packet* &p);
    void recvReply(Packet* &p);
    void sendRequest(Packet* &p);
    void piggybackSourceLocation(Packet *p);
    void piggybackLasthopLocation(Packet *p);
    void evaluateLocation(Packet *p);
    void updateLocation(Packet *p);
    void updatePosition(struct nodelocation &entry, struct hdr_ip* iph=NULL);
    Packet* genRequest(nsaddr_t dst_, int seqno_, int maxhop_);
    Packet* genReply(Packet* req, struct nodelocation* infosrc);
    bool suppressedPkt(Packet* &p);

    inline double bcastDelay() { 
	return (Random::uniform(RLS_MAX_BCAST_DELAY));
    }

    inline double distance(double x1, double y1, double x2, double y2) {
	return sqrt( ((x1 - x2)*(x1 - x2)) + ((y1 - y2)*(y1 - y2)) );
    }
};

#endif // _ReaLocService_h

