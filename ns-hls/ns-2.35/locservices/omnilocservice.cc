/*

  Contacts: 
  Michael Kaesemann (mikael@uni-mannheim.de)

  Permission to use, copy, modify, and distribute this software and its
  documentation in source and binary forms is hereby granted, provided 
  that the above copyright notice appears in all copies and the author is 
  acknowledged in all documentation pertaining to any such copy or
  derivative work.

  THIS SOFTWARE IS PROVIDED ``AS IS'' AND WITHOUT ANY EXPRESS OR
  IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
  WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.

*/

#ifndef _OmniLocService_cc
#define _OmniLocService_cc

#include "omnilocservice.h"

OmniLocService::OmniLocService(Agent* p)
  : LocationService(p)
{}

void OmniLocService::recv(Packet* &p) {
    return;
}

bool OmniLocService::poslookup(Packet *p) {
    struct hdr_ip *iphdr = HDR_IP(p);
    God::instance()->getPosition(iphdr->daddr(), &iphdr->dx_, &iphdr->dy_, &iphdr->dz_);
    return true;
}

#endif // _OmniLocService_cc


