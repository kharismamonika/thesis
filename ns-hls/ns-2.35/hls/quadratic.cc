#include "quadratic.h"
#include <math.h>
#include <stdio.h>

// defines the length of a level-n-region meassured in
// level-n-1-regions 
#define REGION_LENGTH 3 
// defines the heigth of a level-n-region meassured in
// level-n-1-regions 
#define REGION_HEIGTH 3

#define FACTOR 2
// necessary for the V2. REGION_LENTH and .._HEIGTH 
// above now stand for the base size of a region, the
// factor indicates the growth. e.g. lenght = heigth = 3,
// factor = 2: level one Region 3x3, level 2 region 6x6
// level 3 region 12x12 and so on
#define HIGHEST_LEVEL_CENTER_ZONE_SIDELENGTH 1.0
// the sidelength of the area in the center of the 
// simulation field for the highest level rcs (these should
// be in the center of the area)
// meassured in fraction of the total length and heigth
// thus 0 < HIGHEST_LEVEL_CENTER_ZONE_SIDELENGTH <= 1

// the quadratic cellbuilder divides the array in squares
// with a diagonal which is equal to the radio range
// the id's for the cell are assigned from left to right
// and from bottom to top :
//
// +---+---+---+---+---+---+---+---+---+          +
// | 18| 19| 20| 21| 22| 23| 24| 25| 26|
// +---+---+---+---+---+---+---+---+---+
// | 9 | 10| 11| 12| 13| 14| 15| 16| 17|     0
// +---+---+---+---+---+---+---+---+---+
// | 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 |
// +---+---+---+---+---+---+---+---+---+
// +     0     +     1     +     2     +          +    
//


// this cellbuilder is still in a prototype state,
// bugs are possible !!

QuadraticCellbuilder::QuadraticCellbuilder
   (double radiorange, double fieldsize_x, double fieldsize_y)
       : Cellbuilder(radiorange, x, y)
    {
      this->radiorange = radiorange;
      this->x = fieldsize_x;
      this->y = fieldsize_y;
      printf("radiorange %.f, x %.f y %.f\n", radiorange,x,y);

      // determine the length and heigth of one square
      double tmp = sqrt(((radiorange) * (radiorange)) / 2);

      // used to round it to the next lower int
      squarelength = (int) floor(tmp); 
      tmp = x / squarelength;
      // round to the next bigger int
      cellsPerLine = (int) ceil(tmp);

      tmp = y / squarelength;
      cellsPerColumn = (int) ceil(tmp);

      double factorLength, factorHeigth;
      factorLength = (log((double)cellsPerLine/REGION_LENGTH)/ log(2.0))+1;
      factorHeigth = (log((double)cellsPerColumn/REGION_HEIGTH)/log(2.0))+1;
      
      printf("length : %f , cellperline %d , heigth %f\n", factorLength, cellsPerLine, factorHeigth);
      
      if(factorLength > factorHeigth)
	{
	  tmp = factorLength;
	}
      else
	{
	  tmp = factorHeigth;
	}
      maxLevel = (int) ceil(tmp);
      printf("maxlevel %d\n", maxLevel);
    }


// returns the id number of the cell to which the given
// position belongs to
int QuadraticCellbuilder::getCellID (position position)
{
  int line, column;
  line   = (int) (position.y / squarelength);
  column = (int) position.x / squarelength;
  
  return (line*cellsPerLine) + column;
}


// returns the region on level LEVEL to which this position belongs to
int QuadraticCellbuilder::getRegion (position position, int level)
{
  if(level > maxLevel)
    {
      return NO_VALID_LEVEL;
    }

  int cellid = getCellID(position);
  
  int line = cellid / cellsPerLine;
  int column = cellid % cellsPerLine;  
  

  // length and heigth are meassured in numbers of cells
  int length = (int)  pow(2.0, level-1)*REGION_LENGTH;
  int heigth = (int) pow(2.0, level-1)*REGION_HEIGTH;

  column = column / length;
  line = line / heigth;
  int regionsPerLine = (cellsPerLine / length) + 1;

  return (line*regionsPerLine) + column;
}


// returns the responsible cell (RC) on the level for the
// node with NODEID in the region to which position belongs to
// (so it can be determined which cell is responsible for the node
// nodeid in the region to which position belongs to)
int QuadraticCellbuilder::getRC (int nodeid, int level, position position)
{
  if(level > maxLevel)
    {
      return NO_VALID_LEVEL;
    }

  int region = getRegion(position, level);
  
  // length of a region on this level, meassured in cells
  int length = (int)  pow(2.0, level-1)*REGION_LENGTH;

  // heigth of a region on this level, meassured in cells
  int heigth = (int) pow(2.0, level-1)*REGION_HEIGTH;
  
  // compute nodeid mod count of the cells in the region
  // This makes sure competences are evenly distributed
  
  // adding the level to also make the decision depending on it
  int cell = (nodeid + level) % (length*heigth);

  int lineInRegion = cell / length;
  int columnInRegion = cell % length;

  int regionsPerLine = (cellsPerLine) / length + 1;

  int lineOfRegion = ((region / regionsPerLine) * heigth);
  int columnOfRegion = ((region % regionsPerLine) * length);

  int lineAbsolute = lineOfRegion + lineInRegion;
  int columnAbsolute = columnOfRegion + columnInRegion;


  return (lineAbsolute*cellsPerLine) + columnAbsolute;

}


// returns the position of the cell with CELLID
// (the geographical center of the cell)
position QuadraticCellbuilder::getPosition (int cellid)
{
  position tmp = position();
  tmp.x = 0;
  tmp.y = 0;

  int line = cellid / cellsPerLine;
  int column = cellid % cellsPerLine;

  tmp.x = (int) ((column * squarelength) + (0.5 * squarelength))/1;
  tmp.y = (int) ((line * squarelength) + (0.5 * squarelength))/1;

  return tmp;
}


int QuadraticCellbuilder::getMaxLevel()
{
  return maxLevel;
}

// purpose of the function:
// determine the neighbor cells of the cell with cellid. Neighbor-cells are those
// who share an edge with the given cell (in the quadratic version, there at max
// 8 neighbor cells, at min 3 (if the cell is in the edge)
// RETURN: *number contains the number of neighbors, *result an array with the 
// cellids
// !!! copied from CellbuilderV2 !!!
void QuadraticCellbuilder::getNeighborCells(int cellid, int* number, 
					      int** result)
{
  // calculation:
  // +---+---+---+---+
  // | 18| 19| 20| 21| 
  // +---+---+---+---+
  // | 9 | 10| 11| 12|
  // +---+---+---+---+
  // | 0 | 1 | 2 | 3 | 
  // +---+---+---+---+
  // consider we want to have the neighbors of 11:
  // take line (=1) and column (=2) of 11, subtract one (so we
  // have the line (=0) and column (=1) of cell 1 and check the 
  // 9 combinations of line and column (check if they are in our simulation 
  // area).

  int line = cellid / cellsPerLine;
  int column = cellid % cellsPerLine;
  line -= 1; // we start at the lower left edge of the 8 sorounding cells
  column -= 1;
  int localResult[8]; // we have 8 neighbors at max
  int resultCounter = 0;

  int tmpLine = line;
  if(isLineValid(tmpLine)) // check if line is in field
    {
      for(int k=0;k<3;k++) // all 3 columsn
	{
	  int tmpColumn = column + k;
	  if(isColumnValid(tmpColumn))// check if column is in field
	    {
	      // calculate the cellid
	      localResult[resultCounter] = (tmpLine*cellsPerLine) + tmpColumn;
	      resultCounter++;
	    }	      
	}
    }

  tmpLine = line+1;
  // field on the right hand side
  // ...
  // ..x
  // ...
  if(isColumnValid(column+2))// check if column is in field
    {
      // calculate the cellid
      localResult[resultCounter] = (tmpLine*cellsPerLine) + column+2;
      resultCounter++;
    }

  tmpLine = line+2;
  // highest line (from right to left)
  // xxx
  // ...
  // ...
  if(isLineValid(tmpLine)) // check if line is in field
    {
      for(int k=2;k>=0;k--) // all 3 columsn from right to left
	{
	  int tmpColumn = column + k;
	  if(isColumnValid(tmpColumn))// check if column is in field
	    {
	      // calculate the cellid
	      localResult[resultCounter] = (tmpLine*cellsPerLine) + tmpColumn;
	      resultCounter++;
	    }	      
	}
    }

  tmpLine = line+1;
  // and the last on on the left
  // ...
  // x..
  // ...
  if(isColumnValid(column))// check if column is in field
    {
      // calculate the cellid
      localResult[resultCounter] = (tmpLine*cellsPerLine) + column;
      resultCounter++;
    }
  

  
  *result = new int[8];
  
  for(int i=0;i<8;i++)
    {
      if(i<resultCounter)
	{
	  (*result)[i] = localResult[i];
	}
      else
	{
	  (*result)[i] = -1;
	}
    }
  *number = 8;
      
}




//////////////////////////////////////////////////////////
// the second version of the quadratic cellbuilder
// it is a special version for a 2000 x 2000 scenario
// the REGION_LENGTH defines here the size of a level 1 region
// (meassured in cells). The building of the region is here
// top-down, that means the cellbuilder divides the area of 
// the simulation in four, these four in 16 and each of these
// areas is now 3 cells long and high
//////////////////////////////////////////////////////////
QuadraticCellbuilderV2::QuadraticCellbuilderV2
   (double radiorange, double fieldsize_x, double fieldsize_y)
  : Cellbuilder(radiorange, x, y)
    {
      this->radiorange = radiorange;
      this->x = fieldsize_x;
      this->y = fieldsize_y;

      // determine the length and heigth of one square
      double tmp = sqrt(((radiorange) * (radiorange)) / 2);

      // used to round it to the next lower int
      squarelength = (int) floor(tmp); 
      tmp = x / squarelength;
      // round to the next bigger int
      cellsPerLine = (int) ceil(tmp);


      tmp = y / squarelength;
      cellsPerColumn = (int) ceil(tmp);

      numberOfCells = cellsPerLine * cellsPerColumn;

      double factorLength, factorHeigth;
      factorLength = log((double)cellsPerLine)/log((double)REGION_LENGTH);
      factorHeigth = log((double)cellsPerColumn)/log((double)REGION_HEIGTH);
      
      
      if(factorLength > factorHeigth)
	{
	  tmp = factorLength;
	}
      else
	{
	  tmp = factorHeigth;
	}
      maxLevel = (int) ceil(tmp);
    }


// returns the id number of the cell to which the given
// position belongs to
int QuadraticCellbuilderV2::getCellID (position position)
{
  int line, column;
  line   = (int) (position.y / squarelength);
  column = (int) position.x / squarelength;
  
  return (line*cellsPerLine) + column;
}


// returns the region on level LEVEL to which this position belongs to
int QuadraticCellbuilderV2::getRegion (position position, int level)
{
  if(level > maxLevel)
    {
      return NO_VALID_LEVEL;
    }

  if(level == maxLevel)
    {
      return 1;
    }

  int cellid = getCellID(position);
  
  int line = cellid / cellsPerLine;
  int column = cellid % cellsPerLine;  
  

  // length and heigth are meassured in numbers of cells
  int length = (int) (REGION_LENGTH * pow((double)FACTOR, level-1));
  int heigth = (int) (REGION_HEIGTH * pow((double)FACTOR, level-1));

  column = column / length;
  line = line / heigth;
  int regionsPerLine = (cellsPerLine / length) + 1;

  return (line*regionsPerLine) + column;
}


// returns the responsible cell (RC) on the level for the
// node with NODEID in the region to which position belongs to
// (so it can be determined which cell is responsible for the node
// nodeid in the region to which position belongs to)
int QuadraticCellbuilderV2::getRC (int nodeid, int level, position position)
{
  if(level > maxLevel)
    {
      return NO_VALID_LEVEL;
    }
  
  int region = getRegion(position, level);
  
  // length of a region on this level, meassured in cells
  int length; 
  int heigth; 
  if(level < maxLevel)
    {
      length = (int) (REGION_LENGTH * pow((double)FACTOR, level-1));
      // heigth of a region on this level, meassured in cells
      
      heigth = (int) (REGION_HEIGTH * pow((double)FACTOR, level-1));
    }
  else
    {
      // we are on the maxlevel, the rc's on the highest levels
      // should be somewhere in the center of the aread.
      length = (int) ceil(cellsPerLine*HIGHEST_LEVEL_CENTER_ZONE_SIDELENGTH);
      heigth = (int) ceil(cellsPerColumn*HIGHEST_LEVEL_CENTER_ZONE_SIDELENGTH);
    }
  

  // compute nodeid mod count of the cells in the region
  // This makes sure competences are evenly distributed

  int lineAbsolute;
  int columnAbsolute;

  // adding the level to also make the decision depending on it
  int cell = (nodeid + level) % (length*heigth);
  
  int lineInRegion = cell / length;
  int columnInRegion = cell % length;
  
  if(level < maxLevel)
    {      
      int regionsPerLine = (cellsPerLine) / length + 1;
      
      int lineOfRegion = ((region / regionsPerLine) * heigth);
      int columnOfRegion = ((region % regionsPerLine) * length);
      
      lineAbsolute = lineOfRegion + lineInRegion;
      columnAbsolute = columnOfRegion + columnInRegion;
    }
  else
    {
      // maxLevel
      lineAbsolute = ((cellsPerLine-length)/2) + lineInRegion;
      //             < line of region       >
      columnAbsolute = ((cellsPerColumn-heigth)/2) + columnInRegion;
      //                < column of region      >
    }


  // the "% numberOfCells" assures that cell we return is a valid one
  return ((lineAbsolute*cellsPerLine) + columnAbsolute) % numberOfCells;

}


// returns the position of the cell with CELLID
// (the geographical center of the cell)
position QuadraticCellbuilderV2::getPosition (int cellid)
{
  position tmp = position();
  tmp.x = 0;
  tmp.y = 0;

  int line = cellid / cellsPerLine;
  int column = cellid % cellsPerLine;

  tmp.x = (int) ((column * squarelength) + (0.5 * squarelength))/1;
  tmp.y = (int) ((line * squarelength) + (0.5 * squarelength))/1;

  return tmp;
}

int QuadraticCellbuilderV2::getMaxLevel()
{
  return maxLevel;
}

// purpose of the function:
// determine the neighbor cells of the cell with cellid. Neighbor-cells are those
// who share an edge with the given cell (in the quadratic version, there at max
// 8 neighbor cells, at min 3 (if the cell is in the edge)
// RETURN: *number contains the number of neighbors, *result an array with the 
// cellids
void QuadraticCellbuilderV2::getNeighborCells(int cellid, int* number, 
					      int** result)
{
  // calculation:
  // +---+---+---+---+
  // | 18| 19| 20| 21| 
  // +---+---+---+---+
  // | 9 | 10| 11| 12|
  // +---+---+---+---+
  // | 0 | 1 | 2 | 3 | 
  // +---+---+---+---+
  // consider we want to have the neighbors of 11:
  // take line (=1) and column (=2) of 11, subtract one (so we
  // have the line (=0) and column (=1) of cell 1 and check the 
  // 9 combinations of line and column (check if they are in our simulation 
  // area).

  int line = cellid / cellsPerLine;
  int column = cellid % cellsPerLine;
  line -= 1; // we start at the lower left edge of the 8 sorounding cells
  column -= 1;
  int localResult[8]; // we have 8 neighbors at max
  int resultCounter = 0;
  int tmpLine = line;
  
  // first line (from left to right)
  // ...
  // ...
  // xxx
  if(isLineValid(tmpLine)) // check if line is in field
    {
      for(int k=0;k<3;k++) // all 3 columsn
	{
	  int tmpColumn = column + k;
	  if(isColumnValid(tmpColumn))// check if column is in field
	    {
	      // calculate the cellid
	      localResult[resultCounter] = (tmpLine*cellsPerLine) + tmpColumn;
	      resultCounter++;
	    }	      
	}
    }

  tmpLine = line+1;
  // field on the right hand side
  // ...
  // ..x
  // ...
  if(isColumnValid(column+2))// check if column is in field
    {
      // calculate the cellid
      localResult[resultCounter] = (tmpLine*cellsPerLine) + column+2;
      resultCounter++;
    }

  tmpLine = line+2;
  // highest line (from right to left)
  // xxx
  // ...
  // ...
  if(isLineValid(tmpLine)) // check if line is in field
    {
      for(int k=2;k>=0;k--) // all 3 columsn from right to left
	{
	  int tmpColumn = column + k;
	  if(isColumnValid(tmpColumn))// check if column is in field
	    {
	      // calculate the cellid
	      localResult[resultCounter] = (tmpLine*cellsPerLine) + tmpColumn;
	      resultCounter++;
	    }	      
	}
    }

  tmpLine = line+1;
  // and the last on on the left
  // ...
  // x..
  // ...
  if(isColumnValid(column))// check if column is in field
    {
      // calculate the cellid
      localResult[resultCounter] = (tmpLine*cellsPerLine) + column;
      resultCounter++;
    }
  

  
  *result = new int[8];
  
  for(int i=0;i<8;i++)
    {
      if(i<resultCounter)
	{
	  (*result)[i] = localResult[i];
	}
      else
	{
	  (*result)[i] = -1;
	}
    }
  *number = 8;
      
}
