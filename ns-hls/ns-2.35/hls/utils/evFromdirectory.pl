#!/usr/bin/perl
use strict;

my $locs      = "3";
my $mode      = "";
my $nohup     = "";
my $recursive = "false";

##############
# main program
##############
parseCmdLine();
if($mode eq "") {
    printHelp();
    exit;
}
print "test all\n";
if($recursive eq "true") {
    print "recursive\n";
    opendir(DIRECTORY, '.');
    my @entries = readdir(DIRECTORY);
    foreach my $entry (@entries) {
	if(( -d $entry) && (!($entry =~ /\.+/o)) && ($entry =~ /\d+-\d{2}/o) &&
	   (!($entry =~ /\d+-\d{2}-/o))) { # last one necessary because I'm too stupid to exclude those with trailing "-" in the third regexp
	    # select all directories which are != "." or ".."
	    print "processing $entry ...\n";
	    if($mode eq "entire") {
		processEntireDirectory($entry);
	    }
	    if ($mode eq "single") {
		processSingleDirectories($entry);
	    }
	} else {
	    #print "Don't process $entry\n";
	}
    }
} else {
    print "non-recursive\n";
    if($mode eq "entire") {
	processEntireDirectory(".");
    }
    if ($mode eq "single") {
	processSingleDirectories(".");
    }
}
    



# generates for each tracefile an evaluation file
sub processSingleDirectories {
    my $entry = shift();
    chdir $entry;

    opendir(WORKING_DIRECTORY, '.');

    my @dirs = readdir(WORKING_DIRECTORY);
    foreach my $dir (@dirs) {
	if(( -d $dir) && (!($dir =~ /\.+/o))) {
#	    print "$dir\n";
	    #opendir(ACTUAL_DIRECTORY, '$dir');
	    
	    chdir $dir;
	    
	    if($locs eq "3") {
		if(containsEnoughQueries("hls_evaluated_auto", 1200) eq "false") {
		    my $command = "$nohup evaluate.pl -f hls_trace* > hls_evaluated_auto";
		    print "execute in $dir : $command\n";
		    system($command);
		} else {
		    print "$dir contained enough queries\n";
		}
	    } else {		
		if(containsEnoughQueries("gls_evaluated_auto", 1200)  eq "false") {
		    my $command = "$nohup gls_evaluate.pl -f gls_trace* > gls_evaluated_auto";
		    print "execute  in $dir : $command\n";
		    system("$command");
		} else {
		    print "$dir contained enough queries\n";
		}
	    }
	    chdir "..";	    
	}
    }

    chdir "..";
}


# generates one big evaluation file
sub processEntireDirectory {
    my $entry = shift();
    chdir $entry;

    opendir(WORKING_DIRECTORY, '.');
    print "$entry\n";
    if($locs eq "3") {
	my $command = "$nohup evaluate.pl -f */hls_trace* > hls_evaluated_auto";
	print "execute $command\n";
	system($command);
    } else {
	my $command = "$nohup gls_evaluate.pl -f */gls_trace* > gls_evaluated_auto";
	print "execute $command\n";
	system($command);
    }	    

    chdir "..";
}


##### helper functions
# input : filename, nr of queries
# parses the file and returns true if the file contains a line with
# "Queries : $queries" where the latter is the input
sub containsEnoughQueries {
    my $filename = shift();
    my $queries_necessary = shift();
    open(FILE, "$filename");
    my $queries = 0;
    while (my $line = <FILE>) {
	if($line =~ /Queries         : (\d+)/o){
	    $queries = $1;
	}
    }
    if($queries_necessary == $queries) {
	return "true";
    } else {
	return "false";
    }
}


sub parseCmdLine {
  for (my $i = 0; $i <= $#ARGV; $i++) {
      if ($ARGV[$i] eq "-gls") {
	  $locs = "2";
      }
      if ($ARGV[$i] eq "-help") {
	  printHelp();
	  exit;
      }  
      if ($ARGV[$i] eq "-mode") {
	  $mode      = $ARGV[$i+1];
	  $i++;
	  next;
      }
      if ($ARGV[$i] eq "-nohup") {
	  $nohup = "nohup";
      }          
      if ($ARGV[$i] eq "-r") {
	  $recursive = "true";
      }  
  }
}


sub printHelp {

    print "This script is designed to evaluate.pl recursively\n";
    print "-gls    : launch GLS evaluation (DEFAULT : HLS)\n";
    print "-mode   : entire: one big evaluate file; single: one eval file for each trace\n";
    print "-nohup  : nohup\n";
    print "-r      : recursive (DEFAULT :false)\n";
}
