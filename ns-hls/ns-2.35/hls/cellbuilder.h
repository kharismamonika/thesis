#ifndef Cellbuilder_h
#define Cellbuilder_h

#ifdef VISUALIZER
#include "datastructures.h"
#else

#include "../locservices/hdr_locs.h"
#endif
#define NO_VALID_LEVEL -1
/* this is the basic class for building the cells
   it should never be instatiated, it just gives the 
   prototypes of the methods
*/

class Cellbuilder 
{
  public:

  Cellbuilder(double radiorange, double fieldsize_x, double fieldsize_y)
    {
      this->radiorange = radiorange;
      this->x = fieldsize_x;
      this->y = fieldsize_y;
    }
  
  virtual int getCellID (position position){return -1;};
  
  virtual int getRegion (position position, int level){return -1;};
  
  virtual int getRC (int nodeid, int level, position position){return -1;};
  
  virtual position getPosition (int cellid){return position();};

  virtual int getMaxLevel(){return -1;}

  virtual void getNeighborCells(int cellid, int* number, int** result)
    {return;};
  
 private:
  double radiorange;
  double x;
  double y; 
};
#endif

