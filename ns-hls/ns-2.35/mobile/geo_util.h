/*
  This is a collection of functions that were globally defined in
  multiple files.
*/


#ifndef GEO_UTIL_H
#define GEO_UTIL_H

#include "stdio.h"
//#include <vector>

/*#ifndef MIN
#define MIN(a,b) (((a)<(b))?(a):(b))
#endif // MIN*/
#ifndef max
#define max(a,b) (((a)<(b))?(b):(a))
#endif // max

#ifndef SWAP
#define SWAP(a,b) temp=(a);(a)=(b);(b)=temp;
#endif

#ifndef SIGN
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))
#endif

inline void swap(float *a, float *b){
  float temp = *a;
  *a = *b;
  *b = temp;
}


double distance(double x1, double y1, double z1, double x2, double y2,
		double z2);
    
double bearing(double x1, double y1, double x2, double y2);

int cross_segment(double, double, double, double,
		  double, double, double, double,
		  double * = 0, double * = 0);

double norm(double);

bool cut(double, double, double, double, double *, double *);

/** statistical functions */
inline void nrerror(char error[]){
    sprintf("%s", error);
}

#define MAXIT 100
#define EPS 3.0e-7
#define FPMIN 1.0e-30

#define NR_END 1
#define FREE_ARG char*

double gammln(double);
double betacf(double, double, double);
double betai(double, double, double);
double student(double, double);

float select(unsigned long, unsigned long, float *);//vector<float>&);

//double mind(double, double);

inline double mind(double a, double b){
  return a<b?a:b;
}

inline double maxd(double a, double b){
  return a>b?a:b;
}

/*
template<class T> class Vec : public vector<T>{
 public:
  Vec() : vector<T>() { }
  Vec(int s) : vector<T>(s) { }

  T& operator[](int i) { return at(i);}
  const T& operator[](int i) const { return at(i); }
};
*/

/* extern "C"{ */
/* #include <stdio.h> */
/* #include <stdlib.h> */
/* float *vector(long, long); */
/* void free_vector(float *, long, long); */
/* } */


#endif // GEO_UTIL_H
